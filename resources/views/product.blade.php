<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:title" content="Mandaue Stores">
        <meta property="og:description" content="{{ $product->desc}}">
        <meta property="og:image" content="{{ $product->default_image['path_thumbnail'] }}">
        <meta property="og:url" content="{{ $url }}">
        <title>Mandaue Stores</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel=icon type=image/png sizes=96x96 href=../favicon.ico>


    </head>
    <body>
        <div> {{ $product->desc }}</div>
    </body>
</html>
