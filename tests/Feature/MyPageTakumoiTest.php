<?php

namespace Tests\Feature;

use App\Entities\User;
use Tests\TestCase;

class MyPageTakumoiTest extends TestCase
{
    protected $api = "/api/my_page_takumoi/";

    public function testUpdate()
    {

        $this->withoutExceptionHandling();
        $user = User::orderByRaw('RAND()')->first();
        $response = $this->actingAs($user, 'api')->json('PUT', $this->api . rand(1, 20), [
            'family_name' => 'new kk',
        ]);

        $response->assertStatus(200);

    }
}