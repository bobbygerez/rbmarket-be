<?php

namespace Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Entities\User;

class FlowerSentListTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = User::orderByRaw('RAND()')->first();
        $response = $this->json('GET','/api/top-page-flowers-sent-list');
        $response->assertUnauthorized();
        $response = $this->actingAs($user,'api')->json('GET','/api/top-page-flowers-sent-list');
        $response->assertResourceCollection($response);
    }
}