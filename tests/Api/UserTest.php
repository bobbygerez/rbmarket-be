<?php

namespace Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Entities\User;
use Str;
use Hash;

class UserTest extends TestCase
{
    use WithFaker;
    protected $api = "/api/users/";

    public function testIndex(){
        $user = User::orderByRaw('RAND()')->first();
        $response = $this->actingAs($user,'api')->json('GET',$this->api);
        $response->assertResourcePaginated($response);
    }

    public function testCreate(){
        $user = User::orderByRaw('RAND()')->first();
        $faker = $this->faker;
        $data = [
                'takumoi_id' => rand(1, 30),
                'email' => $faker->safeEmail,
                'password' => 'asdf',
                'first_name' => $faker->firstName($gender = null),
                'last_name' => $faker->lastName,
                'furigana_first_name' => $faker->firstName($gender = null),
                'furigana_last_name' => $faker->lastName,
                'postal_code1' => Str::random(3),
                'postal_code2' => Str::random(4),
                'address' => $faker->text($maxNbChars = 60),
                'phone_number' => $faker->tollFreePhoneNumber,
                'registration_date' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null),
                'birthday' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null),
                'sex' => rand(1, 2),
                'premium_member_status' => rand(0, 1),
        ];
        $response = $this->json('POST',$this->api,$data)->assertUnauthorized();
        $response = $this->actingAs($user,'api')->json('POST',$this->api,$data);
        $response->assertResource($response);
    }

    public function testUpdate(){
        $user = User::orderByRaw('RAND()')->first();

        $faker = $this->faker;

        $payload = array_merge($user->toArray(),[
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
        ]);

        $response = $this->json('PUT',$this->api.$user->id,$payload)->assertUnauthorized();

        $response = $this->actingAs($user,'api')->json('PUT',$this->api.$user->id,$payload);

        $response->assertResource($response);
    }
    public function testDelete(){
        $user = User::orderByRaw('RAND()')->first();
        $id = User::orderByRaw('RAND()')->where('id','!=',$user->id)->first()->id;

        $response = $this->json('DELETE',$this->api.$id)->assertUnauthorized();

        $response = $this->actingAs($user,'api')->delete($this->api.$id);
        $response->assertStatus(200);
    }
}
