<?php

namespace Tests\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Entities\User;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = User::orderByRaw("RAND()")->first();

        //FAILURE
        $response = $this->json('POST','/api/login',['email' => $user->email, 'password' => 'password']);
        $response->assertStatus(401);
        $response->assertErrorStructure($response);

        //SUCCESS
        $response = $this->json('POST','/api/login',['email' => $user->email, 'password' => 'asdf']);
        $response->assertStatus(200);

    }
}
