<?php

namespace App\Repo\HomeImage;

use App\Model\HomeImage;
use App\Repo\BaseRepository;
use App\Model\Image;

class HomeImageRepository extends BaseRepository implements HomeImageInterface
{

    public function __construct()
    {

        $this->modelName = new HomeImage();
       
    }

    public function index($request){
        $homeImages = $this->modelName
            ->orderBy('created_at', 'desc')
            ->get();
        
        return $this->paginate($homeImages);

    }  

    public function store($request){

        $hi = $this->create([
            'desc' => $request->desc
        ]);
        $hi = $this->modelName->find($hi->id);
        $this->addImages($hi, $request);
    }

    public function addImages($model, $request){

        $imageableType = 'App\Model\HomeImage';

        if(isset($_FILES["files"]["name"])){
            foreach($_FILES["files"]["name"] as $key=>$tmp_name){
                $name = $_FILES["files"]["name"][$key];
                $file_name= str_random(5) . '-'. str_slug($_FILES["files"]["name"][$key], '-');
                $file_tmp=$_FILES["files"]["tmp_name"][$key];
                $uploadfile = file_get_contents($file_tmp);

                \File::put(public_path() . '/images/uploads/'.$file_name, $uploadfile);

                if ($_FILES["files"]["name"][$key] === $request->is_primary) {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $model->id,
                        'imageable_type' => $imageableType,
                        'is_primary' => true,
                        'name' => $name,
                        'desc' => $name,
                    ]);
                    
                }else{
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $model->id,
                        'imageable_type' =>  $imageableType,
                        'is_primary' => false,
                        'name' => $name,
                        'desc' => $name,
                    ]);
                }
            }
        }

    }

     public function updatee($request){
       $hi = $this->find($request->id);
       
       foreach($hi->images as $img){
            if(!in_array($img->id, $request->ids)){
                $img->delete();
            }
       }
       if($request->ids != null){
            $images = Image::whereIn('id', $request->ids)->get();

            foreach($images as $image){
                if($request->is_primary === $image->name){
                    Image::find($image->id)->update([
                        'is_primary' => true
                    ]);
                }else{
                    Image::find($image->id)->update([
                        'is_primary' => false
                    ]);
                }
            }
       }

       $this->addImages($hi, $request);
       
        $hi->update($request->all());
   
}

    
}
