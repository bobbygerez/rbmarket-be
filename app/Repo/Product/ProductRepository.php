<?php

namespace App\Repo\Product;

use App\Model\Category;
use App\Model\Comment;
use App\Model\Invoice;
use App\Model\Product;
use App\Model\Star;
use App\Repo\BaseRepository;
use Auth;

class ProductRepository extends BaseRepository implements ProductInterface
{

    public function __construct()
    {
        $this->modelName = new Product();
    }

    public function prepend($string, $chunk)
    {
        if (!empty($chunk) && isset($chunk)) {
            return $string . $chunk;
        } else {
            return $string;
        }
    }

    public function categoriesHome($request)
    {
        $take = $request->take;
        $featuredCat = Category::featuredCategory()
            ->with(['images', 'allParent'])
            ->take($take)
            ->get()
            ->map(function ($cat) {
                $url = $cat->optimus_id . '/' . $cat->slug_name;
                if ($cat->allParent) {
                    $chunk = $cat->allParent['optimus_id'] . '/' . $cat->allParent['slug_name'];
                    $url = $this->prepend($chunk . '/', $url);
                    if ($cat->allParent['allParent']) {
                        $chunk = $cat->allParent['allParent']['optimus_id'] . '/' . $cat->allParent['allParent']['slug_name'];
                        $url = $this->prepend($chunk . '/', $url);
                    }
                }
                return [
                    'default_image' => $cat->default_image,
                    'name' => $cat->name,
                    'slug_name' => $cat->slug_name,
                    'url' => $url,
                ];
            })
            ->values()
            ->all();

        $allCat = Category::with(['images', 'allParent'])
            ->get()
            ->map(function ($cat) {
                $url = $cat->optimus_id . '/' . $cat->slug_name;
                if ($cat->allParent) {
                    $chunk = $cat->allParent['optimus_id'] . '/' . $cat->allParent['slug_name'];
                    $url = $this->prepend($chunk . '/', $url);
                    if ($cat->allParent['allParent']) {
                        $chunk = $cat->allParent['allParent']['optimus_id'] . '/' . $cat->allParent['allParent']['slug_name'];
                        $url = $this->prepend($chunk . '/', $url);
                    }
                }
                return [
                    'default_image' => $cat->default_image,
                    'name' => $cat->name,
                    'slug_name' => $cat->slug_name,
                    'url' => $url,
                ];
            })
            ->values()
            ->all();

        $popularProducts = Product::orderByViews()->take($take)->get();

        $parentCat = Category::select(['name', 'id', 'icon'])->where('parent_id', 0)->take($take)->get();
        $parentCat = $parentCat->map(function ($v) {
            $url = $v->optimus_id . '/' . $v->slug_name;
            return [
                'default_image' => $v->default_image,
                'name' => $v->name,
                'slug_name' => $v->slug_name,
                'url' => $url,
            ];
        })->values()->all();
        return [
            'parentCat' => $parentCat,
            'allCat' => $allCat,
            'featuredCategories' => $featuredCat,
            'popularProducts' => $popularProducts,
        ];

    }

    public function create($request)
    {

        $product = $this->modelName->create($request);
        $array = $request['images'];
        foreach ($array as $key => $image) {
            $image = str_replace('data:image/png;base64,', '', $image['dataURL']);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(5) . '.' . 'png';
            \File::put(public_path() . '/images/uploads/' . $imageName, base64_decode($image));

            reset($array);
            //first
            if ($key === key($array)) {
                Image::create([
                    'path' => 'images/uploads/' . $imageName,
                    'imageable_id' => $product->id,
                    'imageable_type' => 'App\Model\Product',
                    'is_primary' => true,
                    'name' => $imageName,
                    'desc' => $request['desc'],
                ]);
            } else {
                Image::create([
                    'path' => 'images/uploads/' . $imageName,
                    'imageable_id' => $product->id,
                    'imageable_type' => 'App\Model\Product',
                    'is_primary' => false,
                    'name' => $imageName,
                    'desc' => $request['desc'],
                ]);
            }

            end($array);
            //Last
            if ($key === key($array)) {
            }

        }
    }

    public function search($request)
    {

        $categories = Category::where('id', $this->optimus()->encode($request->catId))
            ->with(['allChildren'])->get();

        $catIds = $this->mapRecursive($categories);

        $products = $this->modelName
            ->whereHas('category', function ($q) {
                $q->where('is_active', 1);
            })
            ->when(request('catId') != 'undefined', function ($q) use ($catIds) {
                return $q->whereIn('category_id', $catIds);
            })->where('name', 'LIKE', '%' . $request->string . '%')
            ->relTable()
            ->orderBy('created_at', 'desc')
            ->get()
            ->unique('id');

        return $this->paginate($products);
    }

    public function feedback($request)
    {

        $invoice = Invoice::where('invoice_no', $request->invoice_no)->first();
        Comment::create([
            'commentable_id' => $this->removeStringEncode($request->product_id),
            'commentable_type' => 'App\Model\Product',
            'comments' => $request->comment,
            'user_id' => Auth::User()->id,
            'invoice_id' => $invoice->id,
        ]);

        $star = Star::where('invoice_id', $invoice->id)
            ->where('starable_id', $this->removeStringEncode($request->product_id))
            ->where('starable_type', 'App\Model\Product')
            ->first();
        if ($star == null) {
            Star::create([
                'starable_id' => $this->removeStringEncode($request->product_id),
                'starable_type' => 'App\Model\Product',
                'stars' => $request->stars,
                'user_id' => Auth::User()->id,
                'invoice_id' => $invoice->id,
            ]);
        }

    }

}
