<?php

namespace App\Repo\Product;

use App\Model\Branch;
use App\Model\Image;
use App\Model\Product;
use App\Repo\BaseRepository;
use Auth;

class DashboardProductRepository extends BaseRepository implements ProductInterface
{

    public function __construct()
    {
        $this->modelName = new Product();
    }

    public function index($request)
    {
        if (Auth::User()->isSuperAdmin()) {
            return $this
                ->whereLike('name', 'like', '%' . $request->filter . '%')
                ->when($request->categoryId != 'null', function ($q) use ($request) {
                    $q->where('category_id', $request->categoryId);
                })
                ->with('branch.user')->orderBy('created_at', 'desc')->get();
        }
        return $this->whereLike('name', 'like', '%' . $request->filter . '%')
            ->whereHas('branch.user', function ($q) {
                $q->where('id', Auth::User()->id);
            })
            ->when($request->categoryId != 'null', function ($q) use ($request) {
                $q->where('category_id', $request->categoryId);
            })
            ->with('branch.user')->orderBy('created_at', 'desc')->get();
    }

    public function edit($request)
    {

        $product = $this->where('id', $request->id)->reltable()->first();
        $cat1 = $product->category;
        $cat2 = $cat1->allParent;
        $cat3 = $cat2 !== null ? $cat2->allParent : null;
        $categories = collect([$cat1, $cat2, $cat3])->reject(function ($val) {
            return is_null($val);
        })->sortBy('parent_id')->values()->all();

        return [
            'product' => $product,
            'categories' => $categories,
        ];
    }

    //Flatten Recursive
    public function flatten($array)
    {
        $result = [];
        foreach ($array as $item) {
            if (is_array($item)) {
                $result[] = $item['all_parent'];
                $result = array_merge($result, $this->flatten($item['allParent']));
            }

        }
        return array_filter($result);
    }

    public function update($request)
    {
        $product = $this->find($request->optimus_id);
        $productImages = $product->images;

        if ($request->ids) {
            foreach ($productImages as $img) {
                if (!in_array($img->id, $request->ids)) {
                    $img->delete();
                }
            }

        } else {
            foreach ($productImages as $img) {
                $img->delete();
            }
        }

        $this->addImages($product, $request);

        $product->update($request->all());

    }

    public function create($request)
    {

        $product = $this->modelName->create($request->all());
        return $this->addImages($product, $request);

    }

    public function addImages($product, $request)
    {
        if (isset($_FILES["files"]["name"])) {
            foreach ($_FILES["files"]["name"] as $key => $tmp_name) {

                $file_tmp = $_FILES["files"]["tmp_name"][$key];
                $uploadfile = file_get_contents($file_tmp);

                $image = $file_tmp;
                $file_name = $file_name = str_random(5) . '-' . str_replace(' ', '-', $_FILES["files"]["name"][$key]);

                $destinationPath = public_path() . '/images/uploads/';
                $img = \Img::make($file_tmp);
                $img->resize(300, 300, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . 'thumbnail-' . $file_name);

                \File::put(public_path() . '/images/uploads/' . $file_name, $img);
                \File::put(public_path() . '/images/uploads/' . $file_name, $uploadfile);

                if ($_FILES["files"]["name"][$key] === $request->is_primary) {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'thumbnail' => 'images/uploads/' . 'thumbnail-' . $file_name,
                        'imageable_id' => $product->id,
                        'imageable_type' => 'App\Model\Product',
                        'is_primary' => true,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);

                } else {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'thumbnail' => 'images/uploads/' . 'thumbnail-' . $file_name,
                        'imageable_id' => $product->id,
                        'imageable_type' => 'App\Model\Product',
                        'is_primary' => false,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);
                }
            }
        }

    }

    public function userBranches()
    {
        if (Auth::User()->isSuperAdmin()) {
            return Branch::all();
        }

        return Auth::User()->whereHas('branches', function ($q) {
            $q->where('id', Auth::User()->id);
        })->with('branches')->first()->branches;
    }
}
