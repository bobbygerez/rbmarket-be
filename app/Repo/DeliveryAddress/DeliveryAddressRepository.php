<?php

namespace App\Repo\DeliveryAddress;

use App\Model\DeliveryAddress;
use App\Repo\BaseRepository;
use Auth;

class DeliveryAddressRepository extends BaseRepository implements DeliveryAddressInterface
{

    public function __construct()
    {

        $this->modelName = new DeliveryAddress();

    }

    public function index($request)
    {

        $deliveryAddress = $this->modelName
            ->with(['address'])
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->paginate($deliveryAddress);

    }

    public function store($request)
    {

        if ($request->is_default == true) {
            $das = $this->modelName->where('user_id', Auth::User()->id)->get();
            foreach ($das as $d) {
                $x = $this->modelName->where('id', $d->id)->first();
                $x->update([
                    'is_default' => 0,
                ]);
            }
        }
        $newRequest = $request->all();
        $newRequest['user_id'] = Auth::User()->id;
        $da = $this->create($newRequest);

        $da = $this->modelName->where('id', $da->id)->first();
        $da->address()->create($newRequest);

    }

    public function defaultAddress()
    {

        $da = $this->modelName->where('is_default', 1)
            ->with(['address.province', 'address.city', 'address.brgy'])
            ->first();

        $a = $da->address;
        $a['is_default'] = $da->is_default;
        return $a;
    }

    public function selectedAddress($request)
    {

        $das = $this->modelName->where('user_id', Auth::User()->id)->get();
        foreach ($das as $d) {
            $x = $this->modelName->where('id', $d->id)->first();
            $x->update([
                'is_default' => 0,
            ]);
        }

        $da = $this->modelName->where('id', $request->id)->first();
        $da->update([
            'is_default' => 1,
        ]);

    }

}