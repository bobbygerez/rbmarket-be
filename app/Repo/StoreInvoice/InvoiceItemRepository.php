<?php 

namespace App\Repo\StoreInvoice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\InvoiceItem;
use Auth;
use App\Model\Role;

class InvoiceItemRepository extends BaseRepository implements StoreInvoiceInterface{


    public function __construct(){

        $this->modelName = new InvoiceItem();
    
    }

    public function index( $request ){

         $ii = $this->modelName->with(['product'])->get();
         return $this->paginate($ii);

    }

}