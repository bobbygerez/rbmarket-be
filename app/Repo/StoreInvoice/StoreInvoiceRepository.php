<?php

namespace App\Repo\StoreInvoice;

use App\Model\Invoice;
use App\Model\Payment;
use App\Model\StoreInvoice;
use App\Model\User;
use App\Repo\BaseRepository;
use Auth;

class StoreInvoiceRepository extends BaseRepository implements StoreInvoiceInterface
{

    public function __construct()
    {

        $this->modelName = new StoreInvoice();

    }

    public function index($request)
    {

        $user = User::where('id', Auth::User()->id)->first();
        $disabled = false;
        $seller = false;
        $allRoles = $user->roles->pluck('name')->values()->all();
        if ($user->isSuperAdmin()) {
            $si = $this->modelName
                ->with(['invoice', 'branch', 'storeInvoiceProduct.product'])->orderBy('created_at', 'desc')->get();

            if (in_array('Seller', $allRoles)) {
                $disabled = false;
            } elseif (in_array('Super Admin', $allRoles)) {
                $disabled = false;
            } else {
                $disabled = true;
            }
        } else {
            $si = $this->modelName
                ->whereHas('branch.user', function ($q) {
                    $q->where('id', Auth::User()->id);
                })
                ->with(['invoice', 'branch', 'storeInvoiceProduct.product'])->orderBy('created_at', 'desc')->get();

        }

        $si = $si->map(function ($v) use ($disabled) {

            $products = collect($v['storeInvoiceProduct'])->map(function ($v) {
                return $v->amount * $v->qty;
            })->sum();

            return [
                'branch_id' => $v->branch['optimus_id'],
                'optimus_id' => $v->optimus_id,
                'branch' => $v->branch['name'],
                'invoice_no' => $v->invoice['invoice_no'],
                'payment_type' => $v->invoice['desc'],
                'grand_total' => $products,
                'store_invoice_product' => $v['storeInvoiceProduct'],
                'is_approve' => $v->is_approve,
                'is_disapprove' => $v->is_disapprove,
                'created_at' => $v->created_at,
                'disabled' => $disabled,

            ];
        });
        return $this->paginate(collect($si));

    }

    public function show($request)
    {

        $storeInvoice = $this->where('id', $request->id)
            ->with(['invoice.invoiceItems.product'])
            ->first();
        $ii = $storeInvoice->invoice['invoiceItems']->filter(function ($v) use ($request) {
            return $v['product']['branch_id'] == $this->removeStringEncode($request->branchId);
        });

        return response()->json([
            'storeInvoice' => $storeInvoice,
            'invoiceItems' => $ii,
        ]);

    }

    public function storeInvoiceCheck($request)
    {

        $invoice = Invoice::where('invoice_no', $request->invoice)->first();
        $payment = Payment::where('user_id', Auth::User()->id)
            ->where('invoice_id', $invoice->id)
            ->first();

        if ($payment) {
            return true;
        }
        return false;
    }

    public function recursive($array)
    {
        $result = [];
        foreach ($array as $item) {
            if ($item['allChildren'] != null) {
                $result[] = $item['id'];
            }
            $result = array_merge($result, $this->recursive($item['allChildren']));
        }
        return array_filter($result);
    }

}