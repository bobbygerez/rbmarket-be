<?php 

namespace App\Repo\StoreInvoice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\StoreInvoiceProduct;
class StoreInvoiceProductRepository extends BaseRepository implements StoreInvoiceInterface{


    public function __construct(){

        $this->modelName = new StoreInvoiceProduct();
    
    }

    public function index( $request ){

        return $this->where('store_invoice_id', $request->store_invoice_id)->with(['product'])->get();
    }


}