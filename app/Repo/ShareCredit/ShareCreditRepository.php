<?php

namespace App\Repo\ShareCredit;

use App\Model\ShareCredit;
use App\Repo\BaseRepository;

class ShareCreditRepository extends BaseRepository implements ShareCreditInterface
{

    public function __construct()
    {

        $this->modelName = new ShareCredit();

    }

    public function index()
    {

        $request = app('request');
        $shareCredits = $this->modelName
            ->where('code', 'like', '%' . $request->filter . '%')
            ->with(['user'])
            ->get();

        return $this->paginate($shareCredits);

    }
}
