<?php 

namespace App\Repo\Catalog;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Catalog;

class CatalogRepository extends BaseRepository implements CatalogInterface{


    public function __construct(){

        $this->modelName = new Catalog();
    
    }

    public function index($request){

        $catalogs = $this->modelName->with(['products' => function($q){
                $q->orderBy('created_at', 'desc');
            }, 'products.images' => function($q){
                $q->where('is_primary', 1);
            }])->get();
        
        return $catalogs->map(function($v){
            return [
                'name' => $v->name,
                'products' => $v->products->take(5)
            ];
        })->values()->all();

    }

}