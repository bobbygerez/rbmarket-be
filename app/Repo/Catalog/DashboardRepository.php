<?php 

namespace App\Repo\Catalog;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\Catalog;

class DashboardRepository extends BaseRepository implements CatalogInterface{


    public function __construct(){

        $this->modelName = new Catalog();
    
    }

    public function index($request){
        $catalogs = $this->whereLike('name', 'like', '%' . $request->filter . '%')
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->paginate($catalogs);
    }

}