<?php 

namespace App\Repo\DeliveryPrice;

use App\Repo\BaseRepository;
use App\Repo\BaseInterface;
use App\Model\DeliveryPrice;
use Auth;
class DeliveryPriceRepository extends BaseRepository implements DeliveryPriceInterface{


    public function __construct(){

        $this->modelName = new DeliveryPrice();
    
    }

    public function index($request){

        $DeliveryPrice = $this->modelName
            ->where('name', 'like', '%'.$request->filter.'%')
            ->withoutGlobalScope('App\Scopes\DeliveryPriceScope')
            ->get();

        return $this->paginate($DeliveryPrice);

       
    }

    public function store($request){

        $this->defaultTrue($request);
        $this->modelName->create( $request->all() );
    }

    public function updatee( $request ){

        $this->defaultTrue($request);
        $this->where('id', $request->optimus_id)
            ->noGlobal()
            ->first()
            ->update( $request->all() );
    }

    public function defaultTrue($request){

        if( $request->is_default == true){
            $dp = $this->modelName->all();
            foreach($dp as $d){
                $d->is_default = false;
                $d->update();
            }           
        }

    }

}