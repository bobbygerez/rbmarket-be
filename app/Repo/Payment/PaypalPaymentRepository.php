<?php

namespace App\Repo\Payment;

use App\Model\DeliveryAddress;
use App\Model\Payment;
use App\Repo\BaseRepository;
use Auth;

class PaypalPaymentRepository extends BaseRepository implements PaymentInterface
{

    public function __construct()
    {

        $this->modelName = new Payment();

    }

    public function store($request)
    {
        $deliveryAddress = DeliveryAddress::where('is_default', 1)->first();
        $payment = $this->modelName->where('invoice_id', $this->removeStringEncode($request->invoice_id))->first();

        if ($payment == null) {
            $newRequest = $request->all();
            $newRequest['payment_option_id'] = 2;
            $newRequest['user_id'] = Auth::User()->id;
            $newRequest['invoice_id'] = $this->removeStringEncode($request->invoice_id);
            $newRequest['delivery_address_id'] = $deliveryAddress->id;
            $newPayment = $this->modelName->create($newRequest);

            return $this->modelName->where('id', $newPayment->id)->with(['paymentOption', 'invoice.invoiceItems' => function ($q) {
                $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
            }, 'invoice.invoiceItems.product.images', 'invoice.deliveryInvoice'])
                ->first();
        } else {

            return $this->modelName->where('id', $payment->id)
                ->with(['paymentOption', 'invoice.invoiceItems' => function ($q) {
                    $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
                }, 'invoice.invoiceItems.product.images', 'invoice.deliveryInvoice'])
                ->first();
        }

    }

    public function grandTotal($id)
    {

        $payment = $this->modelName->where('id', $id)
            ->with(['invoice.invoiceItems' => function ($q) {
                $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
            }])
            ->first();

        $ii = collect($payment->invoice['invoiceItems'])->map(function ($v) {
            return $v['price'] * $v['qty'];
        })->sum();

        return $ii + $payment->invoice['deliveryInvoice']['price'];
    }

}
