<?php

namespace App\Repo\Payment;

use App\Model\Payment;
use App\Repo\BaseRepository;
use Auth;

class PaymentRepository extends BaseRepository implements PaymentInterface
{

    public function __construct()
    {

        $this->modelName = new Payment();

    }

    public function index($request)
    {

        $payments = $this->modelName->whereHas('invoice', function ($q) use ($request) {
            $q->where('invoice_no', 'like', '%' . $request->filter . '%');
        })
            ->where('is_void', 0)
            ->where('user_id', Auth::User()->id)
            ->when($request->paymentOption != 'null', function ($q) use ($request) {
                $q->where('payment_option_id', $request->paymentOption);
            })
            ->with(['user', 'paymentOption', 'invoice.invoiceItems' => function ($q) {
                $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
            }])
            ->orderBy('created_at', 'desc')
            ->get();

        $payments = $payments->map(function ($v) {
            $total = collect($v->invoice['invoiceItems'])->map(function ($v) {
                return $v['price'] * $v['qty'];
            })->sum();

            return [
                'invoice_no' => $v->invoice['invoice_no'],
                'name' => $v->user['name'],
                'payment_option' => $v->paymentOption['name'],
                'grand_total' => $total + $v->invoice['deliveryInvoice']['price'],
                'is_completed' => $v->is_completed,
                'is_void' => $v->is_void,
                'id' => $v->id,
                'optimus_id' => $v->optimus_id,
                'created_at' => $v->created_at,
            ];
        });

        return $this->paginate($payments);

    }

    public function show($request)
    {

        return $this->where('id', $request->id)
            ->with(['user.address.brgy', 'user.address.city', 'paymentOption', 'invoice.deliveryInvoice', 'deliveryAddress.address', 'invoice.invoiceItems' => function ($q) {
                $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
            }, 'invoice.invoiceItems.product.images'])
            ->first();

    }

    public function grandTotal($request)
    {

        $payment = $this->where('id', $request->id)
            ->with(['invoice.invoiceItems' => function ($q) {
                $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
            }])
            ->first();

        $ii = collect($payment->invoice['invoiceItems'])->map(function ($v) {
            return $v['price'] * $v['qty'];
        })->sum();

        return $ii + $payment->invoice['deliveryInvoice']['price'];
    }

}
