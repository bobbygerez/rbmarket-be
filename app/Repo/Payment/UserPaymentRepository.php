<?php

namespace App\Repo\Payment;

use App\Model\Payment;
use App\Model\User;
use App\Repo\BaseRepository;
use Auth;

class UserPaymentRepository extends BaseRepository implements PaymentInterface
{

    public function __construct()
    {

        $this->modelName = new Payment();

    }

    public function index($request)
    {
        $user = User::where('id', Auth::User()->id)->first();
        $payments = $this->modelName->whereHas('invoice', function ($q) use ($request) {
            $q->where('invoice_no', 'like', '%' . $request->filter . '%');
        })
            ->when($request->paymentOption != 'null', function ($q) use ($request) {
                $q->where('payment_option_id', $request->paymentOption);
            })
            ->with(['user', 'paymentOption', 'invoice.invoiceItems'])
            ->orderBy('created_at', 'desc')
            ->get();

        $payments = $payments->map(function ($v) use ($user) {
            $total = collect($v->invoice['invoiceItems'])->map(function ($v) {
                return $v['price'] * $v['qty'];
            })->sum();

            return [
                'invoice_no' => $v->invoice['invoice_no'],
                'name' => $v->user['name'],
                'payment_option' => $v->paymentOption['name'],
                'grand_total' => $total + $v->invoice['deliveryInvoice']['price'],
                'is_completed' => $v->is_completed,
                'is_void' => $v->is_void,
                'id' => $v->id,
                'optimus_id' => $v->optimus_id,
                'created_at' => $v->created_at,
                'is_admin' => $user->isSuperAdmin(),
            ];
        });

        return $this->paginate($payments);

    }

    public function show($request)
    {

        return $this->where('id', $request->id)
            ->with(['user.address.brgy', 'user.address.city', 'paymentOption', 'invoice.invoiceItems.product.images', 'invoice.deliveryInvoice', 'deliveryAddress.address'])
            ->first();

    }

    public function grandTotal($request)
    {

        $payment = $this->where('id', $request->id)
            ->with(['invoice.invoiceItems'])
            ->first();

        $ii = collect($payment->invoice['invoiceItems'])->map(function ($v) {
            return $v['price'] * $v['qty'];
        })->sum();

        return $ii + $payment->invoice['deliveryInvoice']['price'];
    }

}