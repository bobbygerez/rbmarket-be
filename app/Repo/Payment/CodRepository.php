<?php

namespace App\Repo\Payment;

use App\Model\DeliveryAddress;
use App\Model\DeliveryInvoice;
use App\Model\DeliveryPrice;
use App\Model\Invoice;
use App\Model\InvoiceItem;
use App\Model\Payment;
use App\Repo\BaseRepository;
use Auth;
use Carbon\Carbon;

class CodRepository extends BaseRepository implements PaymentInterface
{

    public function __construct()
    {

        $this->modelName = new Payment();

    }

    public function store($request)
    {

        $deliveryAddress = DeliveryAddress::where('is_default', 1)->first();
        $newRequest = $request->all();
        $newRequest['user_id'] = Auth::User()->id;

        $poOnThisDate = Invoice::where('created_at', '>', Carbon::now()->subDays(1))
            ->get();

        $no = 1;
        if (count($poOnThisDate) > 0) {
            $no = count($poOnThisDate) + 1;
        } else {
            $no = 1;
        }
        $prCode = date('Ymd') . '-' . str_pad($no, 4, '0', STR_PAD_LEFT) . '-' . str_random(5);

        $invoice = Invoice::create([
            'invoice_no' => $prCode,
            'desc' => 'Cash On Delivery',
        ]);

        $newRequest['invoice_id'] = $invoice->id;
        $newRequest['delivery_address_id'] = $deliveryAddress->id;
        $newPayment = $this->modelName->create($newRequest);

        foreach ($request->products as $product) {
            InvoiceItem::create([
                'product_id' => $product['product']['id'],
                'invoice_id' => $invoice->id,
                'price' => $product['product']['discounted_price'],
                'qty' => $product['qty'],
            ]);
        }

        $shipping = DeliveryPrice::get()->first();

        DeliveryInvoice::create([
            'invoice_id' => $invoice->id,
            'delivery_price_id' => $shipping->id,
            'price' => $shipping->amount,
        ]);

        $this->storeInvoices($request, $invoice->id);

        return $this->modelName->where('id', $newPayment->id)
            ->with(['paymentOption', 'invoice.invoiceItems' => function ($q) {
                $q->withoutGlobalScope('App\Scopes\InvoiceItemScope');
            },
                'invoice.invoiceItems.product.images', 'invoice.deliveryInvoice', 'user', 'deliveryAddress.address'])
            ->first();
    }

    public function grandTotal($payment)
    {

        $ii = collect($payment->invoice['invoiceItems'])->map(function ($v) {
            return $v['price'] * $v['qty'];
        })->sum();

        return $ii + $payment->invoice['deliveryInvoice']['price'];
    }

}
