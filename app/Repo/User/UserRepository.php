<?php

namespace App\Repo\User;

use App\Mail\UserRegistration;
use App\Model\Image;
use App\Model\User;
use App\Repo\BaseRepository;
use Hash;
use Illuminate\Support\Facades\Mail;

class UserRepository extends BaseRepository implements UserInterface
{

    public function __construct()
    {

        $this->modelName = new User();

    }

    public function index($request)
    {

        // $branches = $this->whereLike('name', 'like', '%' . $request->filter . '%')
        //     ->orWhere('lastname', 'like', '%' . $request->filter . '%')
        //     ->when(!in_array(0, Auth::User()->roles->pluck('parent_id')->toArray()), function($q){
        //         $q->where('user_id', Auth::User()->id);
        //     })
        //     ->with('user')
        //     ->orderBy('created_at', 'desc')
        //     ->get();

        // return $this->paginate($branches);

    }

    public function usersAdmin($request)
    {

        $address = [];
        foreach (json_decode($request->address) as $k => $v) {
            $address[$k] = $v;
        }

        $user = $this->find($request->id);
        $user->address()->updateOrCreate($address);
        $user->roles()->sync(explode(",", $request->roles));

        $userImages = $user->images;

        if ($request->ids) {
            foreach ($userImages as $img) {
                if (!in_array($img->id, $request->ids)) {
                    $img->delete();
                }
            }

        } else {
            foreach ($userImages as $img) {
                $img->delete();
            }
        }

        $this->addImages($user, $request);

        $user->update($request->all());

    }

    public function store($request)
    {

        $newRequest = $request->all();
        $c = str_random(32);
        $newRequest['activation_code'] = $c;
        $newRequest['password'] = Hash::make($request->password);
        $newUser = $this->create($newRequest);
        if ($request->userType == 'User') {
            $newUser->roles()->attach($newUser->id, [
                'role_id' => 2,
                'user_id' => $newUser->id,
            ]);
        }
        if ($request->userType == 'Seller') {
            $newUser->roles()->attach($newUser->id, [
                'role_id' => 3,
                'user_id' => $newUser->id,
            ]);
        }

        $referer = $_SERVER['HTTP_REFERER'];

        $array = [
            'name' => $request->firstname . ' ' . $request->middlename . ' ' . $request->lastname,
            'email' => $request->email,
            'activation_code' => $referer . '/activation_code/' . $c,
        ];

        Mail::to($request->email)
            ->send(new UserRegistration($array));
    }

    public function activationCode($code)
    {
        $user = $this->modelName->where('activation_code', '=', $code)->first();
        $user->status = 1;
        $user->activation_code = null;
        $user->update();
    }

    public function userProfile($request)
    {
        $address = [];
        foreach (json_decode($request->address) as $k => $v) {
            $address[$k] = $v;
        }

        $user = $this->find($request->id);
        $user->address()->updateOrCreate($address);

        $userImages = $user->images;

        if ($request->ids) {
            foreach ($userImages as $img) {
                if (!in_array($img->id, $request->ids)) {
                    $img->delete();
                }
            }

        } else {
            foreach ($userImages as $img) {
                $img->delete();
            }
        }

        $this->addImages($user, $request);

        $user->update($request->all());

    }

    public function addImages($user, $request)
    {
        if (isset($_FILES["files"]["name"])) {
            foreach ($_FILES["files"]["name"] as $key => $tmp_name) {
                $file_name = str_random(5) . '-' . str_replace(' ', '-', $_FILES["files"]["name"][$key]);
                $file_tmp = $_FILES["files"]["tmp_name"][$key];
                $uploadfile = file_get_contents($file_tmp);

                \File::put(public_path() . '/images/uploads/' . $file_name, $uploadfile);

                if ($_FILES["files"]["name"][$key] === $request->is_primary) {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $user->id,
                        'imageable_type' => 'App\Model\User',
                        'is_primary' => true,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);

                } else {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $user->id,
                        'imageable_type' => 'App\Model\User',
                        'is_primary' => false,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);
                }
            }
        }

    }

}
