<?php

namespace App\Repo\StorePaymentRequest;

use App\Model\Image;
use App\Model\SPRDeliveryReceived;
use App\Model\StorePaymentRequest;
use App\Repo\BaseRepository;

class StorePaymentRequestRepository extends BaseRepository implements StorePaymentRequestInterface
{

    public function __construct()
    {

        $this->modelName = new StorePaymentRequest();

    }

    public function index($request)
    {

        $spr = $this->modelName->with([
            'sprDeliveryReceived.deliveryReceived.drProducts.product',
            'sprDeliveryReceived.deliveryReceived.store',
        ])
            ->orderBy('created_at', 'desc')
            ->get()
            ->map(function ($v) {
                $paid = false;
                if (count($v->images) > 0) {
                    $paid = true;
                }
                return [
                    'id' => $v->optimus_id,
                    'optimus_id' => $v->optimus_id,
                    'grand_total' => $v->grand_total,
                    'is_paid' => $paid,
                    'remarks' => $v->remarks,
                    'created_at' => $v->created_at,
                    'deliveryReceived' => $v->sprDeliveryReceived,
                ];
            });

        return $this->paginate($spr);

    }

    public function store($request)
    {

        $spr = $this->modelName->create($request->all());
        foreach ($request->deliveryReceived as $dr) {
            SPRDeliveryReceived::create([
                'spr_id' => $spr->id,
                'store_id' => $this->removeStringEncode($dr['store_id']),
                'delivery_received_id' => $this->removeStringEncode($dr['value']),
            ]);
        }
    }

    public function updatee($request)
    {

        $spr = $this->where('id', $request->id)->first();

        foreach ($spr->sprDeliveryReceived as $d) {
            $d->delete();
        }

        foreach ($request->deliveryReceived as $dr) {
            SPRDeliveryReceived::create([
                'spr_id' => $spr->id,
                'store_id' => $this->removeStringEncode($dr['store_id']),
                'delivery_received_id' => $this->removeStringEncode($dr['value']),
            ]);
        }

        $spr->update($request->all());

    }

    public function sprUpdate($request)
    {
        $spr = $this->where('id', $request->id)->first();
        $this->deleteImages($spr);

        $this->addImages($spr, $request);
        $spr->update($request->all());
    }

    public function addImages($spr, $request)
    {
        if (isset($_FILES["files"]["name"])) {
            foreach ($_FILES["files"]["name"] as $key => $tmp_name) {
                $file_name = str_random(5) . '-' . str_replace(' ', '-', $_FILES["files"]["name"][$key]);
                $file_tmp = $_FILES["files"]["tmp_name"][$key];
                $uploadfile = file_get_contents($file_tmp);

                \File::put(public_path() . '/images/uploads/' . $file_name, $uploadfile);

                if ($_FILES["files"]["name"][$key] === $request->is_primary) {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $spr->id,
                        'imageable_type' => 'App\Model\StorePaymentRequest',
                        'is_primary' => true,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);

                } else {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $spr->id,
                        'imageable_type' => 'App\Model\StorePaymentRequest',
                        'is_primary' => false,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);
                }
            }
        }

    }

}
