<?php

namespace App\Repo\Category;

use App\Model\Category;
use App\Model\Image;
use App\Repo\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryInterface
{

    public function __construct()
    {

        $this->modelName = new Category();

    }

    public function store($request)
    {
        $category = $this->create($request->all());
        $this->addImages($category, $request);
    }

    public function updatee($request)
    {
        $category = $this->find($request->id);
        $categoryImages = $category->images;

        if ($request->ids) {
            foreach ($categoryImages as $img) {
                if (!in_array($img->id, $request->ids)) {
                    $img->delete();
                }
            }

        } else {
            foreach ($categoryImages as $img) {
                $img->delete();
            }

        }

        $this->addImages($category, $request);

        $category->update($request->all());
    }

    public function mainCategories()
    {
        return $this->modelName->where('parent_id', 0);
    }

    public function subCategories($id)
    {
        return $this->modelName->where('parent_id', $id);
    }

    public function moreCategories($id)
    {
        return $this->modelName->where('parent_id', $id);
    }

    public function categoriesAll()
    {

        return $categories = $this->modelName->with('allChildren')->where('parent_id', 0)->get();

    }

    public function addImages($category, $request)
    {
        if (isset($_FILES["files"]["name"])) {
            foreach ($_FILES["files"]["name"] as $key => $tmp_name) {
                $file_name = str_random(5) . '-' . str_replace(' ', '-', $_FILES["files"]["name"][$key]);
                $file_tmp = $_FILES["files"]["tmp_name"][$key];
                $uploadfile = file_get_contents($file_tmp);

                \File::put(public_path() . '/images/uploads/' . $file_name, $uploadfile);

                if ($_FILES["files"]["name"][$key] === $request->is_primary) {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $category->id,
                        'imageable_type' => 'App\Model\Category',
                        'is_primary' => true,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);

                } else {
                    Image::create([
                        'path' => 'images/uploads/' . $file_name,
                        'imageable_id' => $category->id,
                        'imageable_type' => 'App\Model\Category',
                        'is_primary' => false,
                        'name' => $file_name,
                        'desc' => $file_name,
                    ]);
                }
            }
        }

    }
}
