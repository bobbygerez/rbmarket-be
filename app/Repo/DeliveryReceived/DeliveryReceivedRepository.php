<?php

namespace App\Repo\DeliveryReceived;

use App\Repo\BaseRepository;
use App\Model\DeliveryReceived;
use App\Model\DeliveryReceivedProduct;

class DeliveryReceivedRepository extends BaseRepository implements DeliveryReceivedInterface
{

    public function __construct()
    {

        $this->modelName = new DeliveryReceived();
       
    }

     public function index($request){

        $deliveryReceived = $this->modelName->with(['storeInvoice.invoice', 'store'])
            ->orderBy('created_at', 'desc')
            ->get()
            
            ->map(function($v){
                return [
                    'store' => $v->store['name'],
                    'invoice_no' => $v->storeInvoice['invoice']['invoice_no'],
                    'created_at' => $v->created_at,
                    'optimus_id' => $v->optimus_id
                ];
             })
             ->filter(function ($v) use ($request) {
                if($request->filter != ''){
                    return false !== stripos($v['invoice_no'], $request->filter);
                }
                return true;
             });
             

        return $this->paginate($deliveryReceived);

       
    }

    public function store( $request ){
        $newRequest = $request->all();
        $newRequest['store_id'] = $this->removeStringEncode( $request->store_id );
        $newRequest['store_invoice_id'] = $this->removeStringEncode( $request->store_invoice_id );
        $dr = $this->modelName->create( $newRequest );

        foreach($request->products as $product){
            DeliveryReceivedProduct::create([
                'product_id' => $product['product']['id'],
                'delivery_received_id' => $dr->id,
                'amount' => $product['product']['discounted_price'],
                'qty' => $product['qty']
            ]);
        }
    }

     public function updatee( $request ){

        $dr = $this->where('id', $request->id)->first();
        $newRequest = $request->all();
        $newRequest['store_id'] = $this->removeStringEncode( $request->store_id );
        $newRequest['store_invoice_id'] = $this->removeStringEncode( $request->store_invoice_id );
        
        foreach($dr->drProducts as $p){
            $p->delete();
        }
        
        foreach($request->products as $product){
            DeliveryReceivedProduct::create([
                'product_id' => $product['product']['id'],
                'delivery_received_id' => $dr->id,
                'amount' => $product['product']['discounted_price'],
                'qty' => $product['qty']
            ]);
        }

        $dr = $this->modelName->update( $newRequest );
    }
    
}
