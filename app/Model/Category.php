<?php

namespace App\Model;

use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use Optimuss;

    protected $table = 'categories';
    protected $fillable = [
        'parent_id',
        'name',
        'icon',
        'url',
        'desc',
        'featured',
        'is_active',
    ];
    protected $appends = ['slug_name', 'optimus_id', 'default_image', 'label', 'value'];
    protected $casts = [
        'featured' => 'boolean',
        'is_active' => 'boolean',
    ];

    public function getLabelAttribute()
    {
        return $this->name;
    }
    public function getValueAttribute()
    {
        return $this->id;
    }
    public function getDefaultImageAttribute()
    {

        return collect($this->images)->filter(function ($v) {
            return $v->is_primary == 1;
        })->first();
    }

    public function images()
    {
        return $this->morphMany('App\Model\Image', 'imageable', 'imageable_type', 'imageable_id');
    }

    public function scopeFeaturedCategory($q)
    {
        return $q->where('featured', 1);
    }
    public function parent()
    {
        return $this->hasOne('App\Model\Category', 'id', 'parent_id');
    }
    public function children()
    {

        return $this->hasMany('App\Model\Category', 'parent_id', 'id')
            ->where('is_active', 1);
    }

    public function allChildren()
    {
        return $this->children()->with('allChildren');
    }

    public function allParent()
    {
        return $this->parent()->with('allParent');
    }

    public function setFeaturedAttribute($val)
    {
        $v = 0;
        if ($val == 'true') {
            $v = 1;
        } else {
            $v = 0;
        }
        $this->attributes['featured'] = $v;
    }

    public function setIsActiveAttribute($val)
    {
        $v = 0;
        if ($val == 'true') {
            $v = 1;
        } else {
            $v = 0;
        }
        $this->attributes['is_active'] = $v;
    }
    public function getSlugNameAttribute()
    {
        return str_slug($this->name);
    }

    public function products()
    {
        return $this->hasMany('App\Model\Product', 'category_id', 'id');
    }

    public function scopeRelTable($q)
    {
        return $q->with(['products.images', 'parent']);
    }

}
