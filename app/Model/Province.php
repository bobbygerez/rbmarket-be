<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class Province extends Model
{
    use Optimuss, Globals;
    protected $table = 'provinces';
    protected $appends = ['optimus_id', 'value', 'label'];
    public function region()
    {
        return $this->belongsTo('App\Model\Region');
    }

    public function cities()
    {
        return $this->hasMany('App\Model\City');
    }

    public function getLabelAttribute(){
        return $this->description;
    }

    public function getValueAttribute(){
        return $this->id;
    }

}
