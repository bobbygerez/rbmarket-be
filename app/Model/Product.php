<?php

namespace App\Model;

use App\Model\Category;
use App\Model\UpScalePrice;
use App\Traits\Obfuscate\Optimuss;
use Carbon\Carbon;
use CyrildeWit\EloquentViewable\Contracts\Viewable;
use CyrildeWit\EloquentViewable\InteractsWithViews;
use Illuminate\Database\Eloquent\Model;

class Product extends Model implements Viewable
{

    use Optimuss, InteractsWithViews;
    protected $table = 'products';
    protected $fillable = [
        'branch_id', 'category_id', 'chart_account_id', 'tax_type_id', 'sku', 'barcode', 'name',
        'desc', 'price', 'discount', 'qty', 'catalog_id', 'unit_id', 'is_active',
    ];

    protected $appends = ['discounted_price', 'slug_name', 'category_slug_name', 'optimus_id', 'default_image', 'out_date', 'up_price'];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($product) {
            foreach ($product->images as $img) {
                unlink(public_path() . '/' . $img->path);
                unlink(public_path() . '/' . 'thumbnail-' . $img->path);
            }
            $product->images()->delete();
        });
    }

    public function setIsActiveAttribute($val)
    {
        $v = 0;
        if ($val == 'true') {
            $v = 1;
        } else {
            $v = 0;
        }
        $this->attributes['is_active'] = $v;
    }

    public function comments()
    {
        return $this->morphMany('App\Model\Comment', 'commentable');
    }

    public function stars()
    {
        return $this->morphMany('App\Model\Star', 'starable');
    }

    public function branch()
    {
        return $this->belongsTo('App\Model\Branch');
    }
    public function images()
    {
        return $this->morphMany('App\Model\Image', 'imageable', 'imageable_type', 'imageable_id');
    }

    public function category()
    {
        return $this->hasOne('App\Model\Category', 'id', 'category_id');
    }

    public function unit()
    {
        return $this->hasOne('App\Model\Unit', 'id', 'unit_id');
    }

    public function catalog()
    {
        return $this->hasOne('App\Model\Catalog', 'id', 'catalog_id');
    }

    public function upScale()
    {

        return UpScalePrice::where('id', 1)->first();

    }

    public function getPriceAttribute($val)
    {

        return (float) $val;

    }

    public function getUpPriceAttribute()
    {
        $upScalePrice = $this->upScale();

        $additionalPrice = ($this->price * ($upScalePrice->percent / 100)) + $this->price;

        //ceil round decimal up .2 is 1

        return ceil((float) $additionalPrice);

    }

    public function getDiscountedPriceAttribute()
    {
        $upScalePrice = $this->upScale();

        $discountedPrice = (float) ($this->price - ($this->price * ($this->discount / 100)));

        $additionalPrice = ($discountedPrice * ($upScalePrice->percent / 100));
        //ceil round decimal up .2 is 1
        return ceil((float) $discountedPrice + $additionalPrice);

    }

    public function getSlugNameAttribute()
    {
        return str_slug($this->name);
    }

    public function scopeRelTable($q)
    {
        return $q->with(['images', 'category.allParent', 'branch', 'catalog', 'comments.user', 'stars', 'unit']);
    }

    public function getCategorySlugNameAttribute()
    {
        return $this->category->slug_name;
    }

    public function getDefaultImageAttribute()
    {

        return collect($this->images)->filter(function ($v) {
            return $v->is_primary == 1;
        })->first();
    }

    public function resolveRouteBinding($value)
    {
        return $this->where('id', $this->optimus()->encode($value))->first() ?? abort(404);
    }

    public function getUpdatedAtAttribute($val)
    {
        return Carbon::parse($val)->diffForHumans();
    }

    public function getOutDateAttribute()
    {

        return Carbon::parse($this->updated_at)->diffInDays() >= 7;

    }

}
