<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\InvoiceItemScope;

class InvoiceItem extends Model
{
    protected $table = 'invoice_item';
    protected $fillable = ['invoice_id', 'product_id', 'qty', 'price'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new InvoiceItemScope);
    }

    public function product(){
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }
}
