<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Globals, Optimuss;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'middlename', 'lastname', 'email', 'password', 'status', 'mobile', 'activation_code', 'status', 'facebook_id',
    ];

    protected $appends = ['name', 'optimus_id', 'role_ids', 'default_image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_code',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($user) {
            $user->address()->delete();
            $user->roles()->detach();
        });
    }

    public function images()
    {
        return $this->morphMany('App\Model\Image', 'imageable', 'imageable_type', 'imageable_id');
    }

    public function AauthAcessToken()
    {
        return $this->hasMany('App\Model\OauthAccessToken');
    }
    public function address()
    {
        return $this->morphOne('App\Model\Address', 'addressable');
    }
    public function roles()
    {
        return $this->belongsToMany('App\Model\Role', 'role_user', 'user_id', 'role_id');
    }

    public function branches()
    {
        return $this->hasMany('App\Model\Branch', 'user_id', 'id');
    }

    public function scopeRelTable($q)
    {
        return $q->with(['roles', 'address']);
    }

    public function getNameAttribute()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function getRoleIdsAttribute()
    {

        return $this->roles->pluck('id');
    }

    public function isSuperAdmin()
    {

        return in_array(0, Auth::User()->roles->pluck('parent_id')->toArray());
    }

    public function getFirstnameAttribute($v)
    {
        return ucfirst($v);
    }

    public function getLastnameAttribute($v)
    {
        return ucfirst($v);
    }

    public function getDefaultImageAttribute()
    {

        $image = collect($this->images)->filter(function ($v) {
            return $v->is_primary == 1;
        })->first();

        if ($image) {
            return $image['path_url'];
        }

        $whitelist = array(
            '127.0.0.1',
            '::1',
        );
        if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
            return url('/api/images/login_avatar.png');
        } else {
            return url('/images/login_avatar.png');

        }

    }
}
