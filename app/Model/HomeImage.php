<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;

class HomeImage extends Model
{
    use Optimuss;

    protected $table = 'home_images';
    protected $fillable = ['desc'];
    protected $appends = ['optimus_id', 'desc_display'];

     public static function boot() {
        parent::boot();
        static::deleting(function($hi) {
            foreach($hi->images as $img){
                $img->delete();
            }
        });
    }

    public function images()
    {
        return $this->morphMany('App\Model\Image', 'imageable', 'imageable_type', 'imageable_id');
    }

    public function getDescDisplayAttribute(){
        return substr($this->desc, 0, 100) ; 
    }

}
