<?php

namespace App\Model;

use App\Scopes\ShareCredit\ShareCreditScope;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class ShareCredit extends Model
{

    use Optimuss;
    protected $table = 'share_credits';

    protected $fillable = [
        'user_id', 'code',
    ];

    protected $appends = ['optimus_id'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ShareCreditScope);
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
