<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class StoreInvoice extends Model
{
    use Optimuss, Globals;
    protected $table = 'store_invoices';
    protected $fillable = ['branch_id', 'invoice_id', 'is_approve', 'is_disapprove'];
    protected $appends = ['optimus_id'];
    protected $casts = [
        'is_approve' => 'boolean',
        'is_disapprove' => 'boolean',
    ];

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new BranchScope);
    }

    public function storeInvoiceProduct()
    {
        return $this->hasMany('App\Model\StoreInvoiceProduct', 'store_invoice_id', 'id');
    }

    public function invoice()
    {
        return $this->hasOne('App\Model\Invoice', 'id', 'invoice_id');
    }

    public function branch()
    {
        return $this->hasOne('App\Model\Branch', 'id', 'branch_id');
    }

    public function deliveryReceived()
    {
        return $this->hasMany('App\Model\DeliveryReceived', 'store_invoice_id', 'id');
    }

    public function comments()
    {
        return $this->morphMany('App\Model\Comment', 'commentable');
    }

}