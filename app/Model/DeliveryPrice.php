<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\DeliveryPriceScope;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;

class DeliveryPrice extends Model
{
    
    use Globals, Optimuss;

    protected $table = 'delivery_prices';
    protected $fillable = ['name', 'amount', 'is_default'];
    protected $appends = ['optimus_id'];

    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new DeliveryPriceScope);
    }

    public function getAmountAttribute($val){
        return (float)$val;
    }

    public function scopeNoGlobal($q){
        return $q->withoutGlobalScope('App\Scopes\DeliveryPriceScope');
    }   

    public function getIsDefaultAttribute($val){
        if($val === 1){
            return true;
        }
        return false;
    }

}
