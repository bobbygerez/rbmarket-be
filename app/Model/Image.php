<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'path', 'imageable_id', 'imageable_type',
        'is_primary', 'name', 'desc', 'thumbnail',
    ];

    protected $appends = ['path_url', 'path_thumbnail'];
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getNameAttribute($val)
    {
        //remove extension file.
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', $val);
    }

    public function getPathUrlAttribute()
    {

        $whitelist = array(
            '127.0.0.1',
            '::1',
        );
        if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
            return url('/api/' . $this->path);
        } else {
            return url($this->path);
        }

    }

    public function getPathThumbnailAttribute()
    {

        $whitelist = array(
            '127.0.0.1',
            '::1',
        );
        if ($this->thumbnail) {
            if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                return url('/api/' . $this->thumbnail);
            } else {
                return url($this->thumbnail);
            }

        } else {
            if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
                return url('/api/' . $this->path);
            } else {
                return url($this->path);
            }

        }

    }

}
