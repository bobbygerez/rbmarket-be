<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UpScalePrice extends Model
{

    protected $table = 'upscale_price';

    protected $fillable = ['percent'];
}
