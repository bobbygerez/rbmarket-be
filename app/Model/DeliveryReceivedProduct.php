<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeliveryReceivedProduct extends Model
{
    
    protected $table = 'delivery_received_products';
    protected $fillable = ['delivery_received_id', 'product_id', 'amount', 'qty'];


    public function product(){
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }

    public function invoice(){
        return $this->hasOne('App\Model\Invoice', 'id', 'invoice_id');
    }
}
