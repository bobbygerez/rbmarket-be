<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SPRDeliveryReceived extends Model
{
    
    protected $table = 'spr_delivery_received';
    protected $fillable = ['spr_id', 'store_id', 'delivery_received_id'];

    public function deliveryReceived(){

        return $this->hasOne('App\Model\DeliveryReceived', 'id', 'delivery_received_id');
    }
}
