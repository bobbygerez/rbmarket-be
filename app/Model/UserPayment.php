<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;

class UserPayment extends Model
{
    use Optimuss, Globals;
    protected $table = 'payments';
    protected $fillable = [
        'payer_id',
        'mobile',
        'lat',
        'lng',
        'user_id',
        'payment_option_id',
        'token_id',
        'invoice_id',
        'delivery_address_id',
        'is_void',
        'remarks'
    ];
    protected $appends = ['optimus_id'];
    public static function boot() {
        parent::boot();
        static::deleting(function($payment) {
            foreach($payment->paymentProducts as $p){
                $p->delete();
            }
        });
    }
    public function paymentProducts(){
        return $this->hasMany('App\Model\PaymentProduct', 'payment_id', 'id');
    }

    public function user(){
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }

    public function deliveryAddress(){
        return $this->hasOne('App\Model\DeliveryAddress', 'id', 'delivery_address_id');
    }

    public function paymentOption(){
        return $this->hasOne('App\Model\PaymentOption', 'id', 'payment_option_id');
    }

     public function invoice(){
        return $this->hasOne('App\Model\Invoice', 'id', 'invoice_id');
    }

    public function getLatAttribute($v){
        return (float)$v;
    }

    
    public function getLngAttribute($v){
        return (float)$v;
    }

    public function getIsCompletedAttribute($val){
        if($val === 1){
            return true;
        }
        return false;
    }
    public function getIsVoidAttribute($val){
        if($val === 1){
            return true;
        }
        return false;
    }
}
