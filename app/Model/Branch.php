<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{

    use Optimuss, Globals;
    protected $table = 'branches';
    protected $fillable = [
        'user_id', 'name', 'desc', 'is_active', 'mobile',
    ];
    protected $appends = ['optimus_id', 'value', 'label'];

    protected $casts = [
        'is_active' => 'boolean',
    ];
    public function user()
    {
        return $this->hasOne('App\Model\User', 'id', 'user_id');
    }

    public function address()
    {
        return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function getLabelAttribute()
    {
        return $this->name;
    }

}