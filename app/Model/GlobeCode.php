<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GlobeCode extends Model
{

    protected $table = 'globe_codes';
    protected $fillable = [
        'code', 'phone', 'user_id',
    ];
}
