<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class Brgy extends Model
{
    use Optimuss, Globals;
    protected $table = 'barangays';
    protected $appends = ['optimus_id', 'value', 'label'];

    public function region()
    {
        return $this->belongsTo('App\Model\Region');
    }

    public function province()
    {
        return $this->belongsTo('App\Model\Province');
    }

    public function city()
    {
        return $this->belongsTo('App\Model\City');
    }

    public function country()
    {
        return $this->belongsTo('App\Model\Country');
    }

     public function getLabelAttribute(){
        return $this->description;
    }

    public function getValueAttribute(){
        return $this->id;
    }
}
