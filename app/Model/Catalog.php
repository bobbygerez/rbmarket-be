<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class Catalog extends Model
{
    use Optimuss, Globals;
    protected $table = 'catalogs';
    protected $fillable = ['icon', 'name'];
    protected $appends = ['optimus_id', 'value', 'label'];

    public static function boot() {
        parent::boot();
        static::deleting(function($catalog) {
            // $catalog->address()->delete();
            // $catalog->roles()->detach();
        });
    }

    public function getLabelAttribute(){
        return $this->name;
    }

    public function products(){

        return $this->hasMany('App\Model\Product', 'catalog_id', 'id');
    }
}
