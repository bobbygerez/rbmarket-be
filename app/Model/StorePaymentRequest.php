<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class StorePaymentRequest extends Model
{

    use Optimuss, Globals;
    protected $appends = ['optimus_id'];
    protected $table = 'store_payment_requests';
    protected $fillable = ['grand_total', 'is_paid', 'remarks'];
    protected $casts = [
        'is_paid' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($spr) {
            foreach ($spr->sprDeliveryReceived as $d) {
                $d->delete();
            }
        });
    }

    public function images()
    {
        return $this->morphMany('App\Model\Image', 'imageable', 'imageable_type', 'imageable_id');
    }

    public function sprDeliveryReceived()
    {
        return $this->hasMany('App\Model\SPRDeliveryReceived', 'spr_id', 'id');
    }
}