<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use Optimuss, Globals;
    protected $table = 'invoices';
    protected $fillable = ['invoice_no', 'desc'];

    protected $appends = ['optimus_id', 'label'];

    public function invoiceItems()
    {
        return $this->hasMany('App\Model\InvoiceItem', 'invoice_id', 'id');
    }

    public function deliveryInvoice()
    {
        return $this->hasOne('App\Model\DeliveryInvoice', 'invoice_id', 'id');
    }

    public function getLabelAttribute()
    {
        return $this->invoice_no;
    }

    public function payment()
    {
        return $this->hasOne('App\Model\Payment', 'invoice_id', 'id');
    }

}
