<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StoreInvoiceProduct extends Model
{
     protected $table = 'store_invoice_products';
    protected $fillable = ['store_invoice_id', 'product_id', 'amount', 'qty'];


    public function product(){
        return $this->hasOne('App\Model\Product', 'id', 'product_id');
    }

    public function invoice(){
        return $this->hasOne('App\Model\Invoice', 'id', 'invoice_id');
    }
}
