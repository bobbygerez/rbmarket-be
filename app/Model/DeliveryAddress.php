<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\DeliveryAddressScope;
class DeliveryAddress extends Model
{
    protected $table = 'delivery_address';
    protected $fillable = ['user_id', 'is_default'];
    
    protected static function boot(){
        parent::boot();
        static::addGlobalScope(new DeliveryAddressScope);
    }

    public function address(){
    	return $this->morphOne('App\Model\Address', 'addressable');
    }

    public function getIsDefaultAttribute($v){
        if($v === 1){
            return true;
        }
        return false;
    }
    
}
