<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DeliveryInvoice extends Model
{
    protected $table = 'delivery_invoice';
    protected $fillable = ['delivery_price_id', 'invoice_id', 'price'];
}
