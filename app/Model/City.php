<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Obfuscate\Optimuss;
use App\Traits\Model\Globals;
class City extends Model
{
    use Optimuss, Globals;
    protected $table = 'cities';
    protected $appends = ['optimus_id', 'value', 'label'];

    public function region()
    {
        return $this->belongsTo('App\Model\Region');
    }

    public function province()
    {
        return $this->belongsTo('App\Model\Province');
    }

    public function barangays()
    {
        return $this->hasMany('App\Model\Brgy');
    }

    public function getLabelAttribute(){
        return $this->description;
    }

    public function getValueAttribute(){
        return $this->id;
    }
}
