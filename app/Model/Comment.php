<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\Globals;
class Comment extends Model
{
    use Globals;
    
    protected $table = 'comments';
    protected $fillable = [
        'commentable_id',
        'commentable_type',
        'comments',
        'user_id'
    ];

    public function commentable(){
    	 return $this->morphTo();
    }

    public function user(){
         return $this->hasOne('App\Model\User', 'id', 'user_id');
    }

    public function setCommentsAttribute($value){
       $this->attributes['comments'] = strip_tags($value);
    }
}
