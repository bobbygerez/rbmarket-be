<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Star extends Model
{
    protected $table = 'stars';
    protected $fillable = [
        'starable_id',
        'starable_type',
        'stars',
        'user_id',
        'invoice_id'
    ];

    public function starable(){
    	 return $this->morphTo();
    }

    public function user(){
         return $this->hasOne('App\Model\User', 'id', 'user_id');
    }
}
