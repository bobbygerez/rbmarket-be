<?php

namespace App\Model;

use App\Traits\Model\Globals;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Database\Eloquent\Model;

class DeliveryReceived extends Model
{

    use Optimuss, Globals;
    protected $table = 'delivery_received';
    protected $fillable = [
        'store_invoice_id',
        'store_id',
        'delivery_no',
    ];

    protected $appends = ['optimus_id'];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($dr) {

            foreach ($dr->drProducts as $p) {
                $p->delete();
            }

        });
    }

    public function drProducts()
    {
        return $this->hasMany('App\Model\DeliveryReceivedProduct', 'delivery_received_id', 'id');
    }

    public function storeInvoice()
    {
        return $this->hasOne('App\Model\StoreInvoice', 'id', 'store_invoice_id');
    }

    public function store()
    {
        return $this->hasOne('App\Model\Branch', 'id', 'store_id');
    }

    public function sprDeliveryReceived()
    {

        return $this->hasOne('App\Model\SPRDeliveryReceived', 'spr_id', 'id');
    }
}