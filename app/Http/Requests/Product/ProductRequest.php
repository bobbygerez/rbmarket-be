<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Obfuscate\Optimuss;
class ProductRequest extends FormRequest
{

    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $barcode = '';
        if($this->get('id') != ''){
            $barcode = $this->removeStringEncode($this->get('id'));
        }
        return [
            'barcode' => 'unique:products,barcode,'. $barcode
        ];
    }
}
