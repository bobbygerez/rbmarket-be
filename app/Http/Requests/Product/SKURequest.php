<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Obfuscate\Optimuss;
class SKURequest extends FormRequest
{

    use Optimuss;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sku = '';
        if($this->get('id') != ''){
            $sku = $this->removeStringEncode($this->get('id'));
        }
        return [
            'sku' => 'unique:products,sku,'. $sku
        ];
    }
}
