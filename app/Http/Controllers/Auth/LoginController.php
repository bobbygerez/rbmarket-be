<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\User;
use Auth;
use Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'status' => 1])) {
            return $this->success('email', $request->email);
        }
        if (Auth::attempt(['username' => request('email'), 'password' => request('password'), 'status' => 1])) {
            return $this->success('username', $request->email);
        } else {
            return response()->json(['error' => 'Invalid Username or Password.'], 401);
        }
    }

    public function facebookRegister(Request $request)
    {

        //https: //graph.facebook.com/v3.1/me
        // Initialize session and set URL.
        $url = 'https://graph.facebook.com/v3.1/me?access_token=' . $request->accessToken .
            '&fields=id%2C%20name%2C%20email&method=get&suppress_http_code=1';

        // Initialize session and set URL.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

// Set so curl_exec returns the result instead of outputting it.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// Get the response and close the channel.
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);

        $user = User::where('email', $response->email)->first();
        if ($user) {

            $roleNames = collect($user->roles)->map(function ($v) {
                return $v->name;
            })->values()->all();

            if (in_array('Super Admin', $roleNames)) {

            } else {
                $user->roles()->detach();
                if ($request->userType == 'User') {
                    $user->roles()->attach($user->id, [
                        'role_id' => 2,
                        'user_id' => $user->id,
                    ]);
                } else {
                    $user->roles()->attach($user->id, [
                        'role_id' => 3,
                        'user_id' => $user->id,
                    ]);
                }

            }

            Auth::loginUsingId($user->id);
            return $this->success('email', $response->email);

        } else {
            $newRequest = [];
            $pass = str_random(8);
            $name = explode(' ', $response->name);
            $newRequest['firstname'] = $name[0];
            $newRequest['lastname'] = $name[1];
            $newRequest['email'] = $response->email;
            $newRequest['facebook_id'] = $response->id;
            $newRequest['password'] = Hash::make($pass);
            $newUser = User::create($newRequest);
            if ($request->userType == 'User') {
                $newUser->roles()->attach($newUser->id, [
                    'role_id' => 2,
                    'user_id' => $newUser->id,
                ]);

            } else {
                $newUser->roles()->attach($newUser->id, [
                    'role_id' => 3,
                    'user_id' => $newUser->id,
                ]);

            }

            Auth::loginUsingId($newUser->id);
            return $this->success('email', $response->email);

        }

    }

    public function logout(Request $request)
    {

        if (Auth::check()) {
            Auth::user()->AauthAcessToken()->delete();

            // //https: //graph.facebook.com/v3.1/me
            // // Initialize session and set URL.
            // $url = 'https://www.facebook.com/logout.php?next=https://a2rexpress.com/&access_token=' . $request->fbToken;

            // // Initialize session and set URL.
            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, $url);

            // // Set so curl_exec returns the result instead of outputting it.
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // // Get the response and close the channel.
            // $response = curl_exec($ch);
            // curl_close($ch);
            // $response = json_decode($response);

        }

        return response()->json([
            'success' => true,
        ]);
    }

    public function success($field, $value)
    {
        $request = app()->make('request');
        $user = User::where($field, $value)->first();
        $success['token'] = $user->createToken('MyApp')->accessToken;
        return response()->json([
            'success' => $success,
            'user' => $user,
            'userLogin' => true,
        ]);
    }
}
