<?php

namespace App\Http\Controllers\Api\Category;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Repo\Category\CategoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    protected $category;
    public function __construct(CategoryInterface $category)
    {

        $this->category = $category;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response()->json([
            'categories' => Category::where('parent_id', 0)
                ->with(['allChildren'])
                ->get(),
        ]);
    }

    public function search(Request $request)
    {

        return response()->json([
            'categories' => $this->category
                ->whereNoObfuscate('name', 'LIKE', '%' . $request->search . '%')
                ->where('is_active', 1)
                ->when($request->search === null, function ($q) {
                    return $q->where('parent_id', 0);
                })
                ->with('allChildren')
                ->get(),
        ]);
    }

    public function mainCategories()
    {

        return response()->json([
            'mainCategories' => $this->category->mainCategories()->get(),
        ]);
    }

    public function subCategories(Request $request)
    {

        return response()->json([
            'subCategories' => $this->category->subCategories($request->id)->get(),
        ]);

    }

    public function moreCategories(Request $request)
    {
        return response()->json([
            'moreCategories' => $this->category->moreCategories($request->id)->get(),
        ]);
    }
}
