<?php

namespace App\Http\Controllers\Api\DeliveryAddress;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repo\DeliveryAddress\DeliveryAddressInterface;

class DeliveryAddressController extends Controller
{

    protected $deliveryAddress;
    public function __construct(DeliveryAddressInterface $deliveryAddress){

        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $request = app()->make('request');
        return response()->json([
            'deliveryAddress' => $this->deliveryAddress->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return response()->json([
            'success' => true,
            'is_default' => $this->deliveryAddress->store( $request )
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function defaultAddress(){

        return response()->json([
            'address' => $this->deliveryAddress->defaultAddress()
        ]);
    }

    public function selectedAddress(Request $request){

        $this->deliveryAddress->selectedAddress($request);

        return response()->json([
            'success' => true
        ]);
    }
}
