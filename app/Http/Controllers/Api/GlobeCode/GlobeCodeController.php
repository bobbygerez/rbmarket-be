<?php

namespace App\Http\Controllers\Api\GlobeCode;

use App\Globe\Connect\Oauth;
use App\Http\Controllers\Controller;
use App\Model\GlobeCode;
use Auth;
use Illuminate\Http\Request;

class GlobeCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $globeCode = GlobeCode::where('user_id', Auth::User()->id)->orderBy('created_at', 'desc')->first();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $oauth = new Oauth("gg95H8oRb8tBgcn656TR6gtEag9kHj4y", "ee585844fb519a3c759d07834216b60142078b183df9b633203019d1ebe24c22");

        $oauth->setCode("rIXpdK8Ir8y6pfroAX5CG5ExqugqzLBFBpngqCx4y6bsejXARIxj485fGpEXBh7g4zAuLezMnhGLgqjhXA86EsgaBRGuLGGAKC7de87HKe4eqSo4pGguEGa8RfA7ixRkEapTa6zfazpb6u9r46zSgXeyoHb4G65CeXBx6u488dXsL4geLhLRzLbh5p4BLu8eEjrhyx4q6fxoX5oIyry9rsREnBpCbozzxFn6EynurXA4eCe9y9EfnxdA6I766AbI");
        $token = $oauth->getAccessToken();

        // $payment = new Payment($token);
        // $decoded = json_decode($token, true);

        // $payment->setEndUserId($decoded['subscriber_number']);
        // $payment->setAmount("5.00");
        // $payment->setDescription("Mandaue Stores Payment");
        // $payment->setReferenceCode("12341000023");
        // $payment->setTransactionOperationStatus("Charged");
        // $result = $payment->sendPaymentRequest();

        $loc = new Location($token);
        $loc->setAddress('Mandaue City, Cebu');
        $loc->setRequestedAccuracy('[accuracy]');
        echo $loc->getLocation();

        return response()->json([
            'response' => $result,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
