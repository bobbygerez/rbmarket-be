<?php

namespace App\Http\Controllers\Api\DeliveryReceived;

use App\Http\Controllers\Controller;
use App\Model\StoreInvoice;
use App\Traits\Obfuscate\Optimuss;

class StoreInvoiceController extends Controller
{

    use Optimuss;
    public function index()
    {
        $request = app()->make('request');
        $si = StoreInvoice::where('branch_id', $this->removeStringEncode($request->store_id))
            ->doesntHave('deliveryReceived')
            ->with(['invoice', 'storeInvoiceProduct.product', 'branch'])->get();
        $storeInvoices = $si->map(function ($v) {
            return [
                'label' => $v->invoice['invoice_no'],
                'value' => $v->optimus_id,
                'products' => $v->storeInvoiceProduct,
            ];
        });
        return response()->json([
            'storeInvoices' => $storeInvoices,
        ]);
    }
}