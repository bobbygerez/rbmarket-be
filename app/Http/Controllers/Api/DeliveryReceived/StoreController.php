<?php

namespace App\Http\Controllers\Api\DeliveryReceived;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Branch;

class StoreController extends Controller
{
    public function index(){

        return response()->json([
            'stores' => Branch::all()
        ]);
    }
}
