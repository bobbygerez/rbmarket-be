<?php

namespace App\Http\Controllers\Api\DeliveryReceived;

use App\Http\Controllers\Controller;
use App\Model\DeliveryReceived;
use App\Repo\DeliveryReceived\DeliveryReceivedInterface;
use Illuminate\Http\Request;

class DeliveryReceivedController extends Controller
{
    protected $deliveryReceived;
    public function __construct(DeliveryReceivedInterface $deliveryReceived)
    {

        $this->deliveryReceived = $deliveryReceived;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('index', DeliveryReceived::class);
        return response()->json([
            'deliveryReceived' => $this->deliveryReceived->index(app()->make('request')),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('index', DeliveryReceived::class);
        return response()->json([
            'success' => $this->deliveryReceived->store($request),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->authorize('index', DeliveryReceived::class);
        $dr = $this->deliveryReceived->where('id', $request->id)
            ->with(['storeInvoice.invoice', 'store', 'drProducts.product'])
            ->first();

        return response()->json([
            'deliveryReceived' => $dr,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('index', DeliveryReceived::class);
        return response()->json([
            'success' => $this->deliveryReceived->updatee($request),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('index', DeliveryReceived::class);
        $this->deliveryReceived->where('id', $id)
            ->first()->delete();
        return response()->json([
            'success' => true,
        ]);
    }
}