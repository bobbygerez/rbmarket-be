<?php

namespace App\Http\Controllers\Api\Unit;

use App\Http\Controllers\Controller;
use App\Model\Unit;

class UnitController extends Controller
{

    public function index()
    {
        return response()->json([
            'units' => Unit::all(),
        ]);

    }

}
