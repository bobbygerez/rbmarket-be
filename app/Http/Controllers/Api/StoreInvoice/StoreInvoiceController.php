<?php

namespace App\Http\Controllers\Api\StoreInvoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repo\StoreInvoice\StoreInvoiceInterface;
use App\Model\StoreInvoice;
class StoreInvoiceController extends Controller
{

    protected $storeInvoice;
    public function __construct(StoreInvoiceInterface $storeInvoice){

        $this->storeInvoice = $storeInvoice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         $this->authorize('index', StoreInvoice::class);
        return response()->json([
            'storeInvoices' => $this->storeInvoice->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        return $this->storeInvoice->show( $request );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve(Request $request){

        $si = $this->storeInvoice->where('id', $request->optimusId)->first();
        $si->update([
            'is_approve' => true,
            'is_disapprove' => false
        ]);
    }

    public function disapprove(Request $request){
        $si = $this->storeInvoice->where('id', $request->optimusId)->first();
        $si->update([
            'is_approve' => false,
            'is_disapprove' => true
        ]);
    }

    public function storeInvoiceCheck(Request $request){
        $si = $this->storeInvoice->storeInvoiceCheck($request);

        return response()->json([
            'success' => $si
        ]);
    }   
}
