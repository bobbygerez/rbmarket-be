<?php

namespace App\Http\Controllers\Api\StoreInvoice;

use App\Http\Controllers\Controller;
use App\Model\Comment;
use App\Model\StoreInvoice;
use App\Traits\Obfuscate\Optimuss;
use Auth;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    use Optimuss;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        $si = StoreInvoice::where('id', $this->optimus()->encode($request->store_id))
            ->with(['comments.user'])
            ->first();
        return response()->json([
            'comments' => $si->comments->map(function ($v) {
                return [
                    'current_user_id' => Auth::User()->id,
                    'user_id' => $v->user['id'],
                    'comment' => $v->comments,
                    'user' => $v->user['name'],
                ];
            })->values()->all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Comment::create([
            'commentable_id' => $this->optimus()->encode($request->store_invoice_id),
            'commentable_type' => 'App\Model\StoreInvoice',
            'store_id' => $this->optimus()->encode($request->branch_id),
            'user_id' => Auth::User()->id,
            'comments' => $request->comment,
        ]);
        $si = StoreInvoice::where('id', $this->optimus()->encode($request->store_invoice_id))
            ->with(['comments.user'])
            ->first();
        return response()->json([
            'comments' => $si->comments->map(function ($v) {
                return [
                    'current_user_id' => Auth::User()->id,
                    'user_id' => $v->user['id'],
                    'comment' => $v->comments,
                    'user' => $v->user['name'],
                ];
            })->values()->all(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
