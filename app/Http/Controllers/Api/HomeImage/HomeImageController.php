<?php

namespace App\Http\Controllers\Api\HomeImage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repo\HomeImage\HomeImageInterface;
use App\Model\HomeImage;

class HomeImageController extends Controller
{
    protected $homeImage;
    public function __construct(HomeImageInterface $homeImage){

        $this->homeImage = $homeImage;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        return response()->json([
            'homeImages' => $this->homeImage->index($request)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->homeImage->store($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
         return response()->json([
            'homeImage' => $this->homeImage->where('id', $request->id)->with('images')->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $this->homeImage->updatee($request);
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $hi = $this->homeImage->find( $request->id );
        $hi->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function getHomeImages(){

        return response()->json([
            'homeImage' => $this->homeImage->with('images')->first()
        ]);
    }
}
