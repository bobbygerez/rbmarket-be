<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Payment;
use App\Model\DeliveryPrice;
use App\Model\Invoice;
use Carbon\Carbon;
use App\Model\InvoiceItem;
use App\Model\DeliveryInvoice;
use App\Model\DeliveryAddress;
use App\Traits\Obfuscate\Optimuss;
use App\Repo\Payment\PaymentInterface;
class PaypalPaymentController extends Controller
{

    use Optimuss;
    protected $payment;
    public function __construct(PaymentInterface $payment)
    {
        $this->payment = $payment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        return response()->json([
            'success' => true,
            'payment' => $this->payment->store($request),
            'grandTotal' => $this->payment->grandTotal( $this->payment->store($request)->id )
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function paypal(Request $request){
        $shipping = DeliveryPrice::get()->first();
        $deliveryAddress = DeliveryAddress::where('is_default', 1)->first();

        $provider = \PayPal::setProvider('express_checkout');
        $data = [];

        $items = collect($request->cart)->map(function($v){
            return [
                'name' => $v['product']['name'],
                'price' => number_format($v['product']['discounted_price'], 2, '.', '') ,
                'qty' => $v['qty']
            ];
        })->values()->all();
        
        array_push($items, [
            'name' => 'Shipping',
            'price' => $shipping->amount,
            'qty' => 1
        ]);

        
        
        $poOnThisDate = Invoice::where('created_at', '>', Carbon::now()->subDays(1) )
            ->get();
        
        $no = 1;
        if(count($poOnThisDate) > 0){
            $no = count($poOnThisDate) + 1;
        }else{
            $no = 1;
        }
        $prCode = date('Ymd') . '-' . str_pad($no, 4, '0', STR_PAD_LEFT).'-'. str_random(5);

        $invoice = Invoice::create([
            'invoice_no' => $prCode,
            'desc' => 'Paypal Payment'
        ]);

        foreach($request->cart as $cart){
            InvoiceItem::create([
                'product_id' => $cart['product']['id'],
                'invoice_id' => $invoice->id,
                'price' => $cart['product']['discounted_price'],
                'qty' => $cart['qty']
            ]);
        }

        DeliveryInvoice::create([
            'invoice_id' => $invoice->id,
            'delivery_price_id' => $shipping->id,
            'price' => $shipping->amount
        ]);

        $data['items'] = $items;

        $data['invoice_id'] = $invoice->id ;
        $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
        $data['return_url'] = $request->server('HTTP_ORIGIN').'/payment/success?invoice='. $this->encodeValue($invoice->id). '&deliveryAddress='. $this->encodeValue($deliveryAddress->id);
        $data['cancel_url'] = $request->server('HTTP_REFERER');

        $total = 0;

        foreach($data['items'] as $item) {
            $total += $item['price']*$item['qty'];
        }

        $data['total'] = $total;
       
        $response = $provider->setExpressCheckout($data);

        return response()->json([
            'url' => $response['paypal_link']
        ]);
    }
}
