<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Model\UserPayment;
use App\Repo\Payment\PaymentInterface;
use Illuminate\Http\Request;

class UserPaymentController extends Controller
{

    protected $payment;
    public function __construct(PaymentInterface $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', UserPayment::class);
        return response()->json([
            'payments' => $this->payment->index(app()->make('request')),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->authorize('index', UserPayment::class);
        return response()->json([
            'payment' => $this->payment->show($request),
            'grandTotal' => $this->payment->grandTotal($request),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->authorize('index', UserPayment::class);
        $p = $this->payment->where('id', $request->id)->first();
        $p->update([
            'is_void' => true,
        ]);
        return response()->json([
            'success' => true,
        ]);
    }

    public function unvoid($id)
    {
        $this->authorize('index', UserPayment::class);
        $p = $this->payment->where('id', $id)->first();
        $p->update([
            'is_void' => false,
        ]);
        return response()->json([
            'success' => true,
        ]);
    }
}