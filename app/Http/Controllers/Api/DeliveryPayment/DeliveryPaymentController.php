<?php

namespace App\Http\Controllers\Api\DeliveryPayment;

use App\Http\Controllers\Controller;

class DeliveryPaymentController extends Controller
{
    public function accept()
    {

        return response()->json([
            'success' => true,
        ]);
    }
}
