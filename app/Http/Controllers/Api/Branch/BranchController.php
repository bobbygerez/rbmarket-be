<?php

namespace App\Http\Controllers\Api\Branch;

use App\Http\Controllers\Controller;
use App\Model\Branch;
use App\Repo\Branch\BranchInterface;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    protected $branch;
    public function __construct(BranchInterface $branch)
    {

        $this->branch = $branch;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Branch::class);
        $request = app()->make('request');
        return response()->json([
            'branches' => $this->branch->index($request),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('index', Branch::class);
        return response()->json([
            'success' => $this->branch->create($request),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $this->authorize('index', Branch::class);
        return response()->json([
            'branch' => $this->branch->where('id', $request->id)->with(['address'])->first(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $this->authorize('index', Branch::class);
        $branch = $this->branch->where('id', $request->id)->with(['address'])->first();
        $address = [
            'brgy_id' => null,
            'city_id' => null,
            'province_id' => null,
            'street_lot_blk' => null,
        ];
        if ($branch->address) {
            $address =
                [
                'brgy_id' => [
                    'label' => $branch->address['brgy']['label'],
                    'value' => $branch->address['brgy']['value'],
                ],
                'province_id' => $branch->address['province_id'],
                'city_id' => $branch->address['city_id'],
                'street_lot_blk' => $branch->address['street_lot_blk'],
            ];

        }

        return response()->json([
            'branch' => $branch,
            'address' => $address,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('index', Branch::class);
        $branch = $this->branch->where('id', $id)->first();

        $address = $request->address;
        $address['brgy_id'] = $request->address['brgy_id']['value'];

        if ($branch->address) {
            $branch->address()->update($address);
        } else {
            $branch->address()->create($address);

        }
        $branch->update($request->all());
        return response()->json([
            'success' => $address,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->authorize('index', Branch::class);
        $this->branch->find($request->id)->delete();
        return response()->json([
            'success' => true,
        ]);
    }

    public function getStores()
    {

        return response()->json([
            'stores' => $this->branch->getStores(),
        ]);

    }
}
