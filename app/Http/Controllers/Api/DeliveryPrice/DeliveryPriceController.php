<?php

namespace App\Http\Controllers\Api\DeliveryPrice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\DeliveryPrice\DeliveryPriceInterface;
use App\Model\DeliveryPrice;

class DeliveryPriceController extends Controller
{
    protected $deliveryPrice;
    public function __construct(DeliveryPriceInterface $deliveryPrice){

        $this->deliveryPrice = $deliveryPrice;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('index', DeliveryPrice::class);
        return response()->json([
            'deliveryPrices' => $this->deliveryPrice->index( app()->make('request') )
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('index', DeliveryPrice::class);
        $this->deliveryPrice->store( $request );
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $this->authorize('index', DeliveryPrice::class);
        return response()->json([
            'deliveryPrice' => $this->deliveryPrice
                        ->where('id', $request->id )
                        ->noGlobal()
                        ->first()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('index', DeliveryPrice::class);
        $this->deliveryPrice->updatee( $request );
        return response()->json([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->authorize('index', DeliveryPrice::class);
        $this->deliveryPrice->where('id', $request->id)
            ->noGlobal()
            ->first()
            ->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function cart(){
        return response()->json([
            'deliveryPrice' => $this->deliveryPrice->all()->first()
        ]);
    }
}
