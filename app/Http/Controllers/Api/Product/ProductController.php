<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use App\Repo\Product\ProductInterface;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use optimuss;
    protected $product;

    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response()->json([
            'products' => $this->product->paginate(
                $this->product->relTable()
            ),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->where('id', $id)
            ->with(['images', 'unit'])
            ->first();
        views($product)->record();
        $product2 = $this->product->where('id', $id)
            ->with(['comments.user.images', 'stars'])
            ->first();
        $comments = $product2->comments->map(function ($v) {
            return [
                'comments' => $v->comments,
                'created_at' => $v->created_at,
                'user' => $v->user['name'],
                'user_img' => $v->user['default_image'],
            ];
        })->values()->all();

        return response()->json([
            'product' => $product,
            'stars' => collect($product2->stars)->average('stars'),
            'rating' => count($product2->stars),
            'comments' => $comments,
        ]);
    }

    public function view(Request $request)
    {
        if ($request->product != '') {
            $id = $request->product;
            $product = $this->product->whereNoObfuscate('id', $id)
                ->with(['images', 'unit', 'category'])
                ->first();
            views($product)->record();
            $product2 = $this->product->whereNoObfuscate('id', $id)
                ->with(['comments.user.images', 'stars'])
                ->first();
            $comments = $product2->comments->map(function ($v) {
                return [
                    'comments' => $v->comments,
                    'created_at' => $v->created_at,
                    'user' => $v->user['name'],
                    'user_img' => $v->user['default_image'],
                ];
            })->values()->all();
            return view('product', [
                'product' => $product,
                'stars' => collect($product2->stars)->average('stars'),
                'rating' => count($product2->stars),
                'comments' => $comments,
                'url' => 'https: //mandaue-stores.com/products/' . $product->category->name . '/' . $product->optimus_id . '/' . $product->name,
            ]);

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, Request $request)
    {

        return response()->json([
            'products' => \App\Model\Product::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $product, Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Request $request)
    {
        //
    }

    public function search(Request $request)
    {

        return response()->json([
            'products' => $this->product->search($request),
        ]);
    }

    public function categoriesHome(Request $request)
    {

        return $this->product->categoriesHome($request);
    }

    public function feedback(Request $request)
    {

        $this->product->feedback($request);

        return response()->json([
            'success' => true,
        ]);
    }
}
