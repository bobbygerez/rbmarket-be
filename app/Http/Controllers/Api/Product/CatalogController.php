<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Catalog;

class CatalogController extends Controller
{
   
    public function index(){
        return response()->json([
            'catalogs' => Catalog::all()
        ]);
    }
}
