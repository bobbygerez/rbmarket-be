<?php

namespace App\Http\Controllers\Api\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Requests\Product\SKURequest;
class ProductValidationController extends Controller
{
    public function barcodeValidation(ProductRequest $request){

    }

    public function skuValidation(SKURequest $request){

    }
}
