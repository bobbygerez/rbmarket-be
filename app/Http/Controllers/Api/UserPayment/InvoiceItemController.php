<?php

namespace App\Http\Controllers\Api\UserPayment;

use App\Http\Controllers\Controller;
use App\Model\Invoice;
use App\Model\InvoiceItem;
use Illuminate\Http\Request;

class InvoiceItemController extends Controller
{
    public function destroy(Request $request, $id)
    {

        $redirect = false;
        $invoice = Invoice::where('id', $request->invoiceId)->with(['invoiceItems', 'payment'])->first();
        if (count($invoice->invoiceItems) > 1) {
            InvoiceItem::where('id', $id)->first()->delete();

        } else {
            InvoiceItem::where('id', $id)->first()->delete();
            $invoice->payment()->delete();
            $redirect = true;
        }

        return response()->json([
            'success' => true,
            'redirect' => $redirect,
        ]);
    }
}
