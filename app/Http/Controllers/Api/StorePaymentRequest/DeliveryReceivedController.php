<?php

namespace App\Http\Controllers\Api\StorePaymentRequest;

use App\Http\Controllers\Controller;
use App\Model\DeliveryReceived;
use App\Model\SPRDeliveryReceived;
use App\Traits\Obfuscate\Optimuss;
use Illuminate\Http\Request;

class DeliveryReceivedController extends Controller
{

    use Optimuss;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request = app()->make('request');

        $deliveryReceiveIds = DeliveryReceived::where('store_id', $this->removeStringEncode($request->selectedStore))
            ->get()
            ->pluck('id')->values()->all();

        return response()->json([
            'deliveryReceived' => DeliveryReceived::where('store_id', $this->removeStringEncode($request->selectedStore))
                ->doesntHave('sprDeliveryReceived')
                ->with(['store', 'storeInvoice.invoice', 'drProducts', 'sprDeliveryReceived'])
                ->get()
                ->map(function ($v) {
                    return [
                        'label' => $v->delivery_no,
                        'value' => $v->optimus_id,
                        'store' => $v->store['name'],
                        'store_id' => $v->store['optimus_id'],
                        'invoice_no' => $v->storeInvoice['invoice']['invoice_no'],
                        'total' => collect($v->drProducts)->map(function ($x) {
                            return $x->amount * $x->qty;
                        })->sum(),

                    ];
                }),
            'ids' => $deliveryReceiveIds,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        return response()->json([
            'spr_dr' => SPRDeliveryReceived::where('spr_id', $this->removeStringEncode($request->id))
                ->with(['deliveryReceived.drProducts.product'])
                ->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}