<?php

namespace App\Http\Controllers\Api\StorePaymentRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\DeliveryReceived;
use App\Traits\Obfuscate\Optimuss;
class DeliveryReceivedProductController extends Controller
{

    use Optimuss;
    
    public function index(){

        $request = app()->make('request');
        $dr =  DeliveryReceived::where('id', $this->removeStringEncode($request->drpId) )
                ->with(['drProducts.product', 'storeInvoice.invoice'])
                ->first();
        $products = collect($dr->drProducts)->map(function($v){
            return [
                'id' => $v->product_id,
                'img' => $v->product['default_image']['path_url'],
                'name' => $v->product['name'],
                'sku' => $v->product['sku'],
                'barcode' => $v->product['barcode'],
                'amount' => $v->amount,
                'qty' => $v->qty
            ];
        });
        
        return response()->json([
            'products' =>  $products,
            'invoice' => $dr->storeInvoice['invoice']['invoice_no']
        ]);

    }
}
