<?php

namespace App\Http\Controllers\Api\StorePaymentRequest;

use App\Http\Controllers\Controller;
use App\Model\StorePaymentRequest;
use App\Model\User;
use App\Repo\StorePaymentRequest\StorePaymentRequestInterface;
use Auth;
use Illuminate\Http\Request;

class SPRController extends Controller
{

    protected $spr;
    public function __construct(StorePaymentRequestInterface $spr)
    {

        $this->spr = $spr;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $this->authorize('index', StorePaymentRequest::class);
        return response()->json([
            'spr' => $this->spr->index(app()->make('request')),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('index', StorePaymentRequest::class);
        $this->spr->store($request);
        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //Address must update user address maybe it's empty.
        $user = User::where('id', Auth::User()->id)->with([
            'address',
        ])->first();

        $spr = $this->spr->where('id', $request->id)->with([
            'images',
            'sprDeliveryReceived.deliveryReceived.drProducts.product',
            'sprDeliveryReceived.deliveryReceived.store', 'sprDeliveryReceived.deliveryReceived.storeInvoice.invoice',
        ])
            ->orderBy('created_at', 'desc')
            ->first();

        return response()->json([
            'paymentImages' => $spr->images,
            'spr' => [
                'id' => $spr->optimus_id,
                'grand_total' => $spr->grand_total,
                'is_paid' => $spr->is_paid,
                'remarks' => $spr->remarks,
                'created_at' => $spr->created_at,
                'deliveryReceived' => $spr->sprDeliveryReceived,
            ],
            'userAddress' => $user->address,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

    }

    public function sprUpdate(Request $request, $id)
    {

        $this->spr->sprUpdate($request);
        return response()->json([
            'success' => true,
        ]);
    }
}