<?php

namespace App\Http\Controllers\Api\StorePaymentRequest;

use App\Http\Controllers\Controller;
use App\Model\PaymentRequest;
use App\Model\User;
use App\Repo\StorePaymentRequest\StorePaymentRequestInterface;
use Auth;
use Illuminate\Http\Request;

class PaymentRequestController extends Controller
{

    protected $spr;
    public function __construct(StorePaymentRequestInterface $spr)
    {

        $this->spr = $spr;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', PaymentRequest::class);
        return response()->json([
            'spr' => $this->spr->index(app()->make('request')),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->authorize('index', PaymentRequest::class);
        $this->spr->store($request);
        return response()->json([
            'success' => true,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

        $this->authorize('index', PaymentRequest::class);

        $address = User::where('id', Auth::User()->id)->with([
            'address',
        ])->first();

        $spr = $this->spr->where('id', $request->id)->with([
            'sprDeliveryReceived.deliveryReceived.drProducts.product',
            'sprDeliveryReceived.deliveryReceived.store', 'sprDeliveryReceived.deliveryReceived.storeInvoice.invoice',
        ])
            ->orderBy('created_at', 'desc')
            ->first();

        return response()->json([
            'spr' => [
                'id' => $spr->optimus_id,
                'grand_total' => $spr->grand_total,
                'is_paid' => $spr->is_paid,
                'remarks' => $spr->remarks,
                'created_at' => $spr->created_at,
                'deliveryReceived' => $spr->sprDeliveryReceived,
            ],
            'userAddress' => $address->address,

        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('index', PaymentRequest::class);
        $this->spr->where('id', $request->id)->first()->delete();
        return response()->json([
            'success' => true,
        ]);
    }

    public function prUpdate(Request $request, $id)
    {

        $this->spr->prUpdate($request);
        return response()->json([
            'success' => true,
        ]);
    }
}
