<?php

namespace App\Http\Controllers\Api\StorePaymentRequest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Branch;
use Auth;

class StoreController extends Controller
{
    
    public function index(){

        return response()->json([
            'stores' => Branch::where('user_id', Auth::User()->id)->get()
        ]);
    }
}
