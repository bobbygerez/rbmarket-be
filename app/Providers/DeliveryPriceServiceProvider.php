<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\DeliveryPrice\DeliveryPriceController;
use App\Repo\DeliveryPrice\DeliveryPriceInterface;
use App\Repo\DeliveryPrice\DeliveryPriceRepository;
class DeliveryPriceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(DeliveryPriceController::class)
        ->needs(DeliveryPriceInterface::class)
        ->give(DeliveryPriceRepository::class);
    }
}
