<?php

namespace App\Providers;

use App\Http\Controllers\Api\ShareCredit\ShareCreditController;
use App\Repo\ShareCredit\ShareCreditInterface;
use App\Repo\ShareCredit\ShareCreditRepository;
use Illuminate\Support\ServiceProvider;

class ShareCreditServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(ShareCreditController::class)
            ->needs(ShareCreditInterface::class)
            ->give(ShareCreditRepository::class);

    }
}
