<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model\PaymentRequest' => 'App\Policies\PaymentRequestPolicy',
        'App\Model\StorePaymentRequest' => 'App\Policies\StorePaymentRequestPolicy',
        'App\Model\DeliveryReceived' => 'App\Policies\DeliveryReceivedPolicy',
        'App\Model\StoreInvoice' => 'App\Policies\StoreInvoicePolicy',
        'App\Model\DeliveryPrice' => 'App\Policies\DeliveryPricePolicy',
        'App\Model\Catalog' => 'App\Policies\CatalogPolicy',
        'App\Model\Category' => 'App\Policies\CategoryPolicy',
        'App\Model\Role' => 'App\Policies\RolePolicy',
        'App\Model\User' => 'App\Policies\UserPolicy',
        'App\Model\Product' => 'App\Policies\ProductPolicy',
        'App\Model\Branch' => 'App\Policies\BranchPolicy',
        'App\Model\Payment' => 'App\Policies\MyPaymentPolicy',
        'App\Model\UserPayment' => 'App\Policies\UserPaymentPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Gate::resource('users', 'App\Policies\UserPolicy');
        Gate::resource('dashboard_products', 'App\Policies\ProductPolicy');
        Gate::resource('dashboard_role', 'App\Policies\RolePolicy');
        Gate::resource('dashboard_categories', 'App\Policies\CategoryPolicy');
        Gate::resource('branches', 'App\Policies\BranchPolicy');
        Gate::resource('payments', 'App\Policies\PaymentPolicy');
        Gate::resource('catalogs_dashboard', 'App\Policies\CatalogPolicy');
        Gate::resource('delivery_price', 'App\Policies\DeliveryPricePolicy');
        Gate::resource('my_payments', 'App\Policies\MyPaymentPolicy');
        Gate::resource('user_payments', 'App\Policies\UserPaymentPolicy');
        Gate::resource('store_invoices', 'App\Policies\StoreInvoicePolicy');
        Gate::resource('delivery_received', 'App\Policies\DeliveryReceivedPolicy');
        Gate::resource('spr', 'App\Policies\StorePaymentRequestPolicy');
        Gate::resource('payment_requests', 'App\Policies\PaymentRequestPolicy');
    }
}
