<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\HomeImage\HomeImageController;
use App\Repo\HomeImage\HomeImageInterface;
use App\Repo\HomeImage\HomeImageRepository;

class HomeImageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         $this->app->when(HomeImageController::class)
        ->needs(HomeImageInterface::class)
        ->give(HomeImageRepository::class);
    }
}
