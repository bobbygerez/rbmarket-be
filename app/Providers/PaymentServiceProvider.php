<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Payment\UserPaymentController;
use App\Http\Controllers\Api\Payment\PaymentController;
use App\Http\Controllers\Api\Payment\PaypalPaymentController;
use App\Http\Controllers\Api\Payment\CodController;
use App\Http\Controllers\Api\Payment\PeraPadalaController;
use App\Repo\Payment\PaymentInterface;
use App\Repo\Payment\UserPaymentRepository;
use App\Repo\Payment\PaymentRepository;
use App\Repo\Payment\PaymentMenuRepository;
use App\Repo\Payment\PaypalPaymentRepository;
use App\Repo\Payment\CodRepository;
use App\Repo\Payment\PeraPadalaRepository;
class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(UserPaymentController::class)
        ->needs(PaymentInterface::class)
        ->give(UserPaymentRepository::class);

        $this->app->when(PaymentController::class)
        ->needs(PaymentInterface::class)
        ->give(PaymentRepository::class);

         $this->app->when(CodController::class)
        ->needs(PaymentInterface::class)
        ->give(CodRepository::class);

          $this->app->when(PeraPadalaController::class)
        ->needs(PaymentInterface::class)
        ->give(PeraPadalaRepository::class);

        $this->app->when(PaypalPaymentController::class)
        ->needs(PaymentInterface::class)
        ->give(PaypalPaymentRepository::class);
    }
}
