<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\StoreInvoice\StoreInvoiceProductController;
use App\Http\Controllers\Api\StoreInvoice\StoreInvoiceController;
use App\Http\Controllers\Api\StoreInvoice\InvoiceItemController;
use App\Repo\StoreInvoice\StoreInvoiceInterface;
use App\Repo\StoreInvoice\StoreInvoiceRepository;
use App\Repo\StoreInvoice\InvoiceItemRepository;
use App\Repo\StoreInvoice\StoreInvoiceProductRepository;
class StoreInvoiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(StoreInvoiceController::class)
        ->needs(StoreInvoiceInterface::class)
        ->give(StoreInvoiceRepository::class);

         $this->app->when(InvoiceItemController::class)
        ->needs(StoreInvoiceInterface::class)
        ->give(InvoiceItemRepository::class);

        $this->app->when(StoreInvoiceProductController::class)
        ->needs(StoreInvoiceInterface::class)
        ->give(StoreInvoiceProductRepository::class);
    }
}
