<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\StorePaymentRequest\PaymentRequestController;
use App\Http\Controllers\Api\StorePaymentRequest\SPRController;
use App\Repo\StorePaymentRequest\StorePaymentRequestInterface;
use App\Repo\StorePaymentRequest\StorePaymentRequestRepository;
use App\Repo\StorePaymentRequest\PaymentRequestRepository;
class StorePaymentRequestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(SPRController::class)
        ->needs(StorePaymentRequestInterface::class)
        ->give(StorePaymentRequestRepository::class);

        $this->app->when(PaymentRequestController::class)
        ->needs(StorePaymentRequestInterface::class)
        ->give(PaymentRequestRepository::class);
    }
}
