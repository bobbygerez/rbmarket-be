<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\DeliveryReceived\DeliveryReceivedController;
use App\Repo\DeliveryReceived\DeliveryReceivedInterface;
use App\Repo\DeliveryReceived\DeliveryReceivedRepository;
class DeliveryReceivedServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(DeliveryReceivedController::class)
        ->needs(DeliveryReceivedInterface::class)
        ->give(DeliveryReceivedRepository::class);
    }
}
