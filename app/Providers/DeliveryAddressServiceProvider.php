<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\DeliveryAddress\DeliveryAddressController;
use App\Repo\DeliveryAddress\DeliveryAddressInterface;
use App\Repo\DeliveryAddress\DeliveryAddressRepository;

class DeliveryAddressServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(DeliveryAddressController::class)
        ->needs(DeliveryAddressInterface::class)
        ->give(DeliveryAddressRepository::class);
    }
}
