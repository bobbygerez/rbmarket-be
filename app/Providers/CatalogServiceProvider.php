<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Api\Catalog\CatalogController;
use App\Http\Controllers\Api\Catalog\DashboardController;
use App\Repo\Catalog\CatalogInterface;
use App\Repo\Catalog\CatalogRepository;
use App\Repo\Catalog\DashboardRepository;
class CatalogServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->when(CatalogController::class)
            ->needs(CatalogInterface::class)
            ->give(CatalogRepository::class);
        
        $this->app->when(DashboardController::class)
            ->needs(CatalogInterface::class)
            ->give(DashboardRepository::class);
    }
}
