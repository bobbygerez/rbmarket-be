<?php

namespace App\Scopes\Branch;

use App\Model\Role;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class ApprovedScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        $roles = Role::where('id', Auth::User()->roles->first()->id)
            ->with(['allChildren'])
            ->first();
        $roleIds = array_merge([$roles->parent_id], $this->recursive($roles->allChildren));

        // if(!in_array($roleIds, 0)){
        //     $builder->where('is_approved', 1);
        // }

    }

    public function recursive($array)
    {
        $result = [];
        foreach ($array as $item) {
            if ($item['allChildren'] != null) {
                $result[] = $item['parent_id'];
            }
            $result = array_merge($result, $this->recursive($item['allChildren']));
        }
        return array_filter($result);
    }
}