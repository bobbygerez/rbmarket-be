<?php

namespace App\Scopes;

use App\Model\User;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class DeliveryAddressScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        $user = User::where('id', Auth::User()->id)->first();

        if ($user->isSuperAdmin() == false) {
            $builder->where('user_id', Auth::User()->id);

        }

    }
}
