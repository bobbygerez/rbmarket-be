<?php

namespace App\Scopes\StoreInvoice;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class BranchScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        // $user = User::where('id', Auth::User()->id)->first();

        // if ($user->isSuperAdmin()) {
        //     $roles = Role::where('id', Auth::User()->roles->first()->id)
        //         ->with(['allChildren'])
        //         ->first();
        //     $roleIds = array_merge([$roles->id], $this->recursive($roles->allChildren));

        //     $builder->whereIn('branch_id', $roleIds);

        // } else {

        // }
        // $builder->where('branch_id', 2);

    }

}
