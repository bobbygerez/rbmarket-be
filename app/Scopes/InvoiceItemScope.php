<?php

namespace App\Scopes;

use App\Model\Role;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class InvoiceItemScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {

        $roles = Role::where('id', Auth::User()->roles->first()->id)
            ->with(['allChildren'])
            ->first();
        $roleIds = array_merge([$roles->id], $this->recursive($roles->allChildren));

        $builder->whereHas('product', function ($q) use ($roleIds) {
            $q->whereIn('branch_id', $roleIds);
        });

    }

    public function recursive($array)
    {
        $result = [];
        foreach ($array as $item) {
            if ($item['allChildren'] != null) {
                $result[] = $item['id'];
            }
            $result = array_merge($result, $this->recursive($item['allChildren']));
        }
        return array_filter($result);
    }
}
