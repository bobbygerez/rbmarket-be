<?php

Route::post('login', 'Auth\LoginController@login');
Route::post('facebook_register', 'Auth\LoginController@facebookRegister');

Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser');

Route::group(['namespace' => 'Auth', 'prefix' => 'password'], function () {
    Route::post('create', 'ResetPasswordController@create');
    Route::get('find/{token}', 'ResetPasswordController@find');

});
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@getDetails');
Route::post('password/reset/{token}', 'Auth\ResetPasswordController@newPassword');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');
    Route::resource('users', 'Api\User\UserController');
    Route::post('users_admin/{id}', 'Api\User\UserController@usersAdmin');
    Route::get('profile', 'Api\User\UserController@profile');
    Route::post('user_profile/{id}', 'Api\User\UserController@userProfile');
    Route::post('profile-update/{id}', 'Api\User\UserController@profileUpdate');
    Route::resource('roles', 'Api\Role\RoleController');
    // Route::resource('role_menu', 'Api\Role\RoleMenuController');
    Route::resource('role_menus', 'Api\Role\MenuController');
    Route::resource('dashboard_categories', 'Api\Category\DashboardCategoryController');
    Route::post('dashboard_categories_update/{id}', 'Api\Category\DashboardCategoryController@update');
    Route::get('categories-all', 'Api\Category\DashboardCategoryController@categoriesAll');
    Route::resource('dashboard_role', 'Api\Role\DashboardRoleController');
    Route::resource('menus', 'Api\Menu\MenuController');
    Route::get('search-menus', 'Api\Menu\MenuController@search');
    Route::resource('dashboard_menus', 'Api\Menu\DashboardMenuController');
    Route::resource('access_rights', 'Api\AccessRight\AccessRightController');
    Route::post('users/change-password', 'Api\User\UserController@changePassword');
    Route::get('get-stores', 'Api\Branch\BranchController@getStores');
    Route::resource('branches', 'Api\Branch\BranchController');
    Route::get('get-main-categories', 'Api\Category\CategoryController@mainCategories');
    Route::get('get-sub-categories', 'Api\Category\CategoryController@subCategories');
    Route::get('get-more-categories', 'Api\Category\CategoryController@moreCategories');
    Route::resource('dashboard_products', 'Api\Product\DashboardProductController');
    Route::post('dashboard-products-update/{id}', 'Api\Product\DashboardProductController@productUpdate');
    Route::resource('my_payments', 'Api\Payment\PaymentController');
    Route::resource('user_payments', 'Api\Payment\UserPaymentController');
    Route::resource('payment_globe', 'Api\Payment\GlobePaymentController');
    Route::resource('globe_codes', 'Api\GlobeCode\GlobeCodeController');
    Route::post('user_payments_unvoid/{id}', 'Api\Payment\UserPaymentController@unvoid');
    Route::resource('payments_payment_options', 'Api\Payment\PaymentOptionController');
    Route::resource('delivery_address', 'Api\DeliveryAddress\DeliveryAddressController');
    Route::get('default_address', 'Api\DeliveryAddress\DeliveryAddressController@defaultAddress');
    Route::post('selected_address', 'Api\DeliveryAddress\DeliveryAddressController@selectedAddress');
    Route::resource('payments_cod', 'Api\Payment\CodController');
    Route::resource('payments_pera_padala', 'Api\Payment\PeraPadalaController');
    Route::resource('paypal_payments', 'Api\Payment\PaypalPaymentController');
    Route::post('paypal_payments_create', 'Api\Payment\PaypalPaymentController@paypal');
    Route::resource('catalogs_dashboard', 'Api\Catalog\DashboardController');
    Route::resource('home_images', 'Api\HomeImage\HomeImageController');
    Route::resource('products_catalogs', 'Api\Product\CatalogController');
    Route::resource('checkout_address', 'Api\Checkout\AddressController');
    Route::post('home_images_update/{id}', 'Api\HomeImage\HomeImageController@update');
    Route::resource('delivery_price', 'Api\DeliveryPrice\DeliveryPriceController');
    Route::resource('store_invoices', 'Api\StoreInvoice\StoreInvoiceController');
    Route::get('store_invoice_check', 'Api\StoreInvoice\StoreInvoiceController@storeInvoiceCheck');
    Route::resource('si_invoice_items', 'Api\StoreInvoice\InvoiceItemController');
    Route::resource('si_store_invoice_products', 'Api\StoreInvoice\StoreInvoiceProductController');
    Route::post('si_approve', 'Api\StoreInvoice\StoreInvoiceController@approve');
    Route::post('si_disapprove', 'Api\StoreInvoice\StoreInvoiceController@disapprove');
    Route::resource('si_comments', 'Api\StoreInvoice\CommentController');
    Route::resource('delivery_received', 'Api\DeliveryReceived\DeliveryReceivedController');
    Route::resource('dr_stores', 'Api\DeliveryReceived\StoreController');
    Route::resource('dr_store_invoices', 'Api\DeliveryReceived\StoreInvoiceController');
    Route::resource('spr_delivery_received', 'Api\StorePaymentRequest\DeliveryReceivedController');
    Route::resource('spr_stores', 'Api\StorePaymentRequest\StoreController');
    Route::resource('spr_drp', 'Api\StorePaymentRequest\DeliveryReceivedProductController');
    Route::resource('spr', 'Api\StorePaymentRequest\SPRController');
    Route::post('spr_update/{id}', 'Api\StorePaymentRequest\SPRController@sprUpdate');
    Route::post('pr_update/{id}', 'Api\StorePaymentRequest\PaymentRequestController@prUpdate');
    Route::resource('payment_requests', 'Api\StorePaymentRequest\PaymentRequestController');
    Route::post('payment_requests_update/{id}', 'Api\StorePaymentRequest\PaymentRequestController@updateImage');
    Route::post('product_feedback', 'Api\Product\ProductController@feedback');
    Route::get('units', 'Api\Unit\UnitController@index');
    Route::resource('users_payments_invoice_item', 'Api\UserPayment\InvoiceItemController');
    Route::resource('share-credits', 'Api\ShareCredit\ShareCreditController');
    Route::post('delivery_payments_accept', 'Api\DeliveryPayment\DeliveryPaymentController@accept');

});

Route::get('get_home_images', 'Api\HomeImage\HomeImageController@getHomeImages');
Route::resource('catalogs', 'Api\Catalog\CatalogController');
/*** Single field validations */
Route::get('user/validator/username', 'Api\User\UserValidationController@userName');
Route::get('user/validator/email', 'Api\User\UserValidationController@email');
Route::get('user/validator/mobile', 'Api\User\UserValidationController@mobile');
Route::get('product/validator/barcode', 'Api\Product\ProductValidationController@barcodeValidation');
Route::get('product/validator/sku', 'Api\Product\ProductValidationController@skuValidation');

Route::post('register/{activation_code}', 'Api\User\UserController@activationCode');
Route::post('user_register', 'Api\User\UserController@store');
Route::get('delivery_price_cart', 'Api\DeliveryPrice\DeliveryPriceController@cart');
Route::resource('payment_options', 'Api\PaymentOption\PaymentOptionController');
Route::resource('products', 'Api\Product\ProductController');
Route::get('categories_home', 'Api\Product\ProductController@categoriesHome');
Route::get('product-search', 'Api\Product\ProductController@search');
Route::get('provinces', 'Api\Places\PlacesController@provinces');
Route::get('cities/{provinceId}', 'Api\Places\PlacesController@cities');
Route::get('brgys/{cityId}', 'Api\Places\PlacesController@brgys');
Route::resource('categories', 'Api\Category\CategoryController');
Route::get('search-category', 'Api\Category\CategoryController@search');
Route::resource('category_products', 'Api\Product\CategoryProductController');
Route::resource('subcategory_products', 'Api\Product\SubCategoryProductController');
Route::resource('subcategory_products', 'Api\Product\SubCategoryProductController');
