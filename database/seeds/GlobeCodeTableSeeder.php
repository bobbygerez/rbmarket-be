<?php

use App\Model\GlobeCode;
use Illuminate\Database\Seeder;

class GlobeCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $code = 'ACGdzr5I9X9pMUy99bLCygkgBf4BnLAI9MLq6UX4AajU6dEbgfzE5GKH5b856t5ykgEf86oBXCdnzq9C5oaRySkdkbeuz6agqhAdMRofpg7xbHa4Rpxu8zXAzhMoT4dGB4McXMKhB5RGauA47a4H5XMb8fkKaekhqpk8EuyqaKMSLzzMjCbXoRLCz8ko6fzk8nKtyX5R6HMoEbEfMgAj6UE8LBgUaxnR4I86k9LfL999yCoA9x8URKzkrIKz647C';
        GlobeCode::create([
            'code' => $code,
            'phone' => '9993088141',
            'user_id' => 1,
        ]);
    }
}
