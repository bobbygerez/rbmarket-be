<?php

use App\Model\ShareCredit;
use Illuminate\Database\Seeder;

class ShareCreditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; $i++) {
            ShareCredit::create([
                'user_id' => $i,
                'code' => $i . '-' . str_random(6),
            ]);
        }
    }
}
