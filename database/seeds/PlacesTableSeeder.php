<?php

use Illuminate\Database\Seeder;

class PlacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // =============================================================
        // file Path -> Project/app/configs/database.php
        // get the database name, database username, database password
        // =============================================================
        $db = \Config::get('database.connections.mysql.database');
        $user = \Config::get('database.connections.mysql.username');
        $pass = \Config::get('database.connections.mysql.password');

        // $this->command->info($db);
        // $this->command->info($user);
        // $this->command->info($pass);

        // running command line import in php code
        if (!Schema::hasTable('provinces')) {
            exec("mysql -u " . $user . " -p" . $pass . " " . $db . " < sql\provinces.sql");
        }
        if (!Schema::hasTable('cities')) {
            exec("mysql -u " . $user . " -p" . $pass . " " . $db . " < sql\cities.sql");
        }
        if (!Schema::hasTable('barangays')) {
            exec("mysql -u " . $user . " -p" . $pass . " " . $db . " < sql\barangays.sql");
        }

    }
}