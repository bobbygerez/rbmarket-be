<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $user = User::create([
            'firstname' => 'super',
            'lastname' => 'admin ',
            'middlename' => 'middlename',
            'mobile' => $faker->phoneNumber,
            'status' => 1,
            'email' => 'admin@iitcebu.net',
            'password' => Hash::make('23456789'),
        ]);
        $newUser = User::find($user->id);
        $user->roles()->attach($user->id, [
            'user_id' => $user->id,
            'role_id' => 1,
        ]);
        $user = User::create([
            'firstname' => 'store',
            'lastname' => 'store',
            'middlename' => 'store',
            'mobile' => $faker->phoneNumber,
            'email' => 'store@iitcebu.net',
            'password' => Hash::make('23456789'),
            'status' => 1,
        ]);
        $newUser = User::find($user->id);
        $newUser->roles()->attach($user->id, [
            'user_id' => $user->id,
            'role_id' => 2,
        ]);

        $user = User::create([
            'firstname' => 'user',
            'lastname' => 'user',
            'middlename' => 'user',
            'mobile' => $faker->phoneNumber,
            'email' => 'user@iitcebu.net',
            'password' => Hash::make('23456789'),
            'status' => 1,
        ]);
        $newUser = User::find($user->id);
        $newUser->roles()->attach($user->id, [
            'user_id' => $user->id,
            'role_id' => 2,
        ]);

    }
}
