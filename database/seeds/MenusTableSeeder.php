<?php

use App\Model\Menu;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            0 => ['path' => '/dashboard/profile', 'icon' => 'account_circle', 'name' => 'Profile'],
            1 => ['path' => '/dashboard/users', 'icon' => 'supervisor_account', 'name' => 'Users'],
            2 => ['path' => '/dashboard/roles', 'icon' => 'recent_actors', 'name' => 'Roles'],
            3 => ['path' => '/dashboard/categories', 'icon' => 'list', 'name' => 'Categories'],
            4 => ['path' => '/dashboard/catalogs', 'icon' => 'fas fa-stream', 'name' => 'Catalogs'],
            5 => ['path' => '/dashboard/delivery-price', 'icon' => 'fas fa-truck', 'name' => 'Delivery Price'],
            6 => ['path' => '/dashboard/stores', 'icon' => 'account_balance', 'name' => 'Stores'],
            7 => ['path' => '/dashboard/products', 'icon' => 'laptop_mac', 'name' => 'Products'],
            8 => ['path' => '/dashboard/my-payments', 'icon' => 'attach_money', 'name' => 'My Payments'],
            9 => ['path' => '/dashboard/users-payments', 'icon' => 'fas fa-hand-holding-usd', 'name' => 'Users Payments'],
            10 => ['path' => '/dashboard/purchased-invoices', 'icon' => 'fas fa-file-invoice-dollar', 'name' => 'Purchased Invoices'],
            11 => ['path' => '/dashboard/delivery-received', 'icon' => 'fas fa-truck-loading', 'name' => 'Delivery Received'],
            12 => ['path' => '/dashboard/store-payment-requests', 'icon' => 'far fa-money-bill-alt', 'name' => 'Store Payment Requests'],
            13 => ['path' => '/dashboard/payment-requests', 'icon' => 'far fa-money-bill-alt', 'name' => 'Payment Requests'],
            14 => ['path' => '/dashboard/share-credits', 'icon' => 'fas fa-share-alt', 'name' => 'Share Credits'],
            15 => ['path' => '/dashboard/my-share-credits', 'icon' => 'fas fa-share-alt', 'name' => 'My Share Credits'],
        ];

        foreach ($menus as $k => $v) {
            Menu::create([
                'icon' => $v['icon'],
                'parent_id' => 0,
                'path' => $v['path'],
                'name' => $v['name'],
            ]);
        }
    }
}
