<?php

use Illuminate\Database\Seeder;

class AddColumnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::table('products', function ($table) {
            if (!Schema::hasColumn('products', 'unit_id')) {
                $table->integer('unit_id')->nullable();
            }

        });

        Schema::table('branches', function ($table) {
            if (!Schema::hasColumn('branches', 'is_active')) {
                $table->boolean('is_active');
            }

        });

        Schema::table('categories', function ($table) {
            if (!Schema::hasColumn('categories', 'is_active')) {
                $table->boolean('is_active');
            }

        });

        Schema::table('products', function ($table) {
            if (!Schema::hasColumn('products', 'is_active')) {
                $table->boolean('is_active');
            }

        });

    }
}
