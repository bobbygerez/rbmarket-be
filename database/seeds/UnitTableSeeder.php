<?php

use App\Model\Unit;
use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $units = ['Pc/Pcs', 'Kilo', 'Bottle', 'Grams', 'Case', 'Liter', "ML", "Gal", "Pack"];

        foreach ($units as $unit) {
            Unit::create([
                'name' => $unit,
            ]);
        }
    }
}
