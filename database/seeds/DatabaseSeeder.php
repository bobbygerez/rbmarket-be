<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // $this->call(AddColumnTableSeeder::class);
        // $this->call(UpScaleTableSeeder::class);
        // $this->call(UnitTableSeeder::class);
        // $this->call(PlacesTableSeeder::class);
        // $this->call(HoldingsTableSeeder::class);
        // $this->call(CompaniesTableSeeder::class);
        // $this->call(TaccountsTableSeeder::class);
        // $this->call(ChartAccountsTableSeeder::class);
        // $this->call(RolesTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(BranchesTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        // $this->call(TaxTypeTableSeeder::class);
        // $this->call(CatalogsTableSeeder::class);
        // $this->call(ProductsTableSeeder::class);
        // $this->call(MenusTableSeeder::class);
        // $this->call(ShareCreditTableSeeder::class);
        // $this->call(RoleMenuTableSeeder::class);
        // $this->call(PaymentOptionTableSeeder::class);
        // $this->call(DeliveryPriceTableSeeder::class);
        $this->call(GlobeCodeTableSeeder::class);

    }
}
