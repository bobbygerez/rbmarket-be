<?php

use Illuminate\Database\Seeder;
use App\Model\UpScalePrice;

class UpScaleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        UpScalePrice::create([
            'percent' => 15
        ]);
    }
}
