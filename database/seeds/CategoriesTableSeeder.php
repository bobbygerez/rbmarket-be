<?php

use App\Model\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
        $categories = [
            'computer' => 'Consumer Electronics',
            'perm_data_setting' => 'Machinery',
            'extension' => 'Apparel',
            'directions_car' => 'Automobiles & Motorcyles',
            'local_florist' => 'Home & Garden',
            'child_care' => 'Beauty & Personal Care',
            'local_hospital' => 'Health & Medical',
            'pool' => 'Sports & Entertainment',
            'far fa-gem' => 'Jewelry and Watches',

        ];

        $computerElectronics = [
            'Camera & Accessories',
            'Mobile Phone & Parts',
            'Computer HardWare &Software',
            'Smart Electronics',
            'Video Games',
        ];

        foreach ($categories as $key => $value) {
            if ($key == 'far fa-gem') {
                Category::create([
                    'parent_id' => 0,
                    'name' => $value,
                    'icon' => $key,
                    'url' => str_slug($value, '-'),
                    'featured' => 1,
                ]);
                $category = Category::find($category->id);
                $category->images()->create([
                    'path' => 'images/uploads/cov' . rand(1, 12) . '.jpg',
                    'is_primary' => 1,
                    'name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                    'desc' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                ]);
            } else {
                $category = Category::create([
                    'parent_id' => 0,
                    'name' => $value,
                    'icon' => $key,
                    'url' => str_slug($value, '-'),
                    'featured' => rand(0, 1),
                ]);

                $category = Category::find($category->id);
                $category->images()->create([
                    'path' => 'images/uploads/cov' . rand(1, 12) . '.jpg',
                    'is_primary' => 1,
                    'name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                    'desc' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                ]);
            }

        }

        foreach ($computerElectronics as $key => $value) {
            Category::create([
                'parent_id' => 1,
                'name' => $value,
                'url' => str_slug($value, '-'),
            ]);
        }

        $cameraAndAccessories = [
            'Camera Filters', 'Camera Flash Lights', 'Camera Lenses', 'Camera Straps', 'Digital Cameras',
            'Digital Photo Frames', 'Film Cameras', 'Films Mini Camcorders'];

        foreach ($cameraAndAccessories as $value) {
            Category::create([
                'parent_id' => 9,
                'name' => $value,
                'url' => str_slug($value, '-'),
            ]);
        }

    }
}