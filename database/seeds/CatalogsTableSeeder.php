<?php

use Illuminate\Database\Seeder;
use App\Model\Catalog;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catalogs = [

            'fas fa-hand-holding-usd' => 'Flash Sale',
            'fas fa-universal-access' => 'Most Popular',
            'fas fa-stream' => 'Collections',
            'fas fa-portrait' => 'Just For You'

        ];

        foreach($catalogs as $k => $v){
            Catalog::create([
                'icon' => $k,
                'name' => $v
            ]);
        }
    }
}
