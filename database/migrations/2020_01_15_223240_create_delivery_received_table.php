<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_received', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('store_invoice_id')->unsigned()->nullable();
            $table->foreign('store_invoice_id')->references('id')
                    ->on('store_invoices');
            $table->integer('store_id')->unsigned()->nullable();
            $table->foreign('store_id')->references('id')
                    ->on('branches');
            $table->string('delivery_no')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_received');
    }
}
