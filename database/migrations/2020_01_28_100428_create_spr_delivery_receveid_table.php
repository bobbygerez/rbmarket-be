<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSprDeliveryReceveidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spr_delivery_received', function (Blueprint $table) {
            $table->bigIncrements('id');
           $table->bigInteger('spr_id')->unsigned()->nullable();
            $table->foreign('spr_id')->references('id')
                    ->on('store_payment_requests');  
            $table->integer('store_id')->unsigned()->nullable();
            $table->foreign('store_id')->references('id')
                    ->on('branches');
            $table->bigInteger('delivery_received_id')->unsigned()->nullable();
            $table->foreign('delivery_received_id')->references('id')
                    ->on('delivery_received');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spr_delivery_received');
    }
}
