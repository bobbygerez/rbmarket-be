<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('mobile');
            $table->string('lat');
            $table->string('lng');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')
                ->on('users');
            $table->bigInteger('payment_option_id')->unsigned()->nullable();
            $table->foreign('payment_option_id')->references('id')
                    ->on('payment_options');
            $table->string('payer_id')->nullable();
            $table->string('token_id')->nullable();
            $table->bigInteger('invoice_id')->unsigned()->nullable();
            $table->foreign('invoice_id')->references('id')
                ->on('invoices');
            $table->bigInteger('delivery_address_id')->unsigned()->nullable();
            $table->foreign('delivery_address_id')->references('id')
                ->on('delivery_address');
            $table->boolean('is_completed')->default(false);
            $table->boolean('is_void')->default(false);
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
